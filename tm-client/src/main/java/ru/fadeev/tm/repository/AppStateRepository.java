package ru.fadeev.tm.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.fadeev.tm.api.endpoint.Role;
import ru.fadeev.tm.api.endpoint.Session;
import ru.fadeev.tm.api.repository.IAppStateRepository;
import ru.fadeev.tm.command.AbstractCommand;
import ru.fadeev.tm.entity.AppState;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public final class AppStateRepository implements IAppStateRepository {

    @NotNull
    private final AppState appState = new AppState();

    @Nullable
    public Role getRole() {
        @Nullable final Session session = appState.getSession();
        return session == null ? null : session.getRole();
    }

    @Override
    public void putCommand(@Nullable final String description, @Nullable final AbstractCommand abstractCommand) {
        appState.getCommands().put(description, abstractCommand);
    }

    @Nullable
    public AbstractCommand getCommand(@Nullable final String command) {
        return appState.getCommands().get(command);
    }

    @NotNull
    public List<AbstractCommand> getCommands() {
        return new ArrayList<>(appState.getCommands().values());
    }

    @Override
    public boolean hasPermission(@NotNull final Role... roles) {
        @Nullable final List<Role> roleList = Arrays.asList(roles);
        return roleList.contains(getRole());
    }

    @Nullable
    public Session getSession(){
        return appState.getSession();
    }

    public void setSession(@Nullable final Session session){
        appState.setSession(session);
    }

}