
package ru.fadeev.tm.api.endpoint;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the ru.fadeev.tm.api.endpoint package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _Exception_QNAME = new QName("http://endpoint.api.tm.fadeev.ru/", "Exception");
    private final static QName _LoadFromBinary_QNAME = new QName("http://endpoint.api.tm.fadeev.ru/", "loadFromBinary");
    private final static QName _LoadFromBinaryResponse_QNAME = new QName("http://endpoint.api.tm.fadeev.ru/", "loadFromBinaryResponse");
    private final static QName _LoadFromJsonFasterXml_QNAME = new QName("http://endpoint.api.tm.fadeev.ru/", "loadFromJsonFasterXml");
    private final static QName _LoadFromJsonFasterXmlResponse_QNAME = new QName("http://endpoint.api.tm.fadeev.ru/", "loadFromJsonFasterXmlResponse");
    private final static QName _LoadFromJsonJaxb_QNAME = new QName("http://endpoint.api.tm.fadeev.ru/", "loadFromJsonJaxb");
    private final static QName _LoadFromJsonJaxbResponse_QNAME = new QName("http://endpoint.api.tm.fadeev.ru/", "loadFromJsonJaxbResponse");
    private final static QName _LoadFromXmlFasterxml_QNAME = new QName("http://endpoint.api.tm.fadeev.ru/", "loadFromXmlFasterxml");
    private final static QName _LoadFromXmlFasterxmlResponse_QNAME = new QName("http://endpoint.api.tm.fadeev.ru/", "loadFromXmlFasterxmlResponse");
    private final static QName _LoadFromXmlJaxb_QNAME = new QName("http://endpoint.api.tm.fadeev.ru/", "loadFromXmlJaxb");
    private final static QName _LoadFromXmlJaxbResponse_QNAME = new QName("http://endpoint.api.tm.fadeev.ru/", "loadFromXmlJaxbResponse");
    private final static QName _SaveToBinary_QNAME = new QName("http://endpoint.api.tm.fadeev.ru/", "saveToBinary");
    private final static QName _SaveToBinaryResponse_QNAME = new QName("http://endpoint.api.tm.fadeev.ru/", "saveToBinaryResponse");
    private final static QName _SaveToJsonFasterXml_QNAME = new QName("http://endpoint.api.tm.fadeev.ru/", "saveToJsonFasterXml");
    private final static QName _SaveToJsonFasterXmlResponse_QNAME = new QName("http://endpoint.api.tm.fadeev.ru/", "saveToJsonFasterXmlResponse");
    private final static QName _SaveToJsonJaxb_QNAME = new QName("http://endpoint.api.tm.fadeev.ru/", "saveToJsonJaxb");
    private final static QName _SaveToJsonJaxbResponse_QNAME = new QName("http://endpoint.api.tm.fadeev.ru/", "saveToJsonJaxbResponse");
    private final static QName _SaveToXmlFasterxml_QNAME = new QName("http://endpoint.api.tm.fadeev.ru/", "saveToXmlFasterxml");
    private final static QName _SaveToXmlFasterxmlResponse_QNAME = new QName("http://endpoint.api.tm.fadeev.ru/", "saveToXmlFasterxmlResponse");
    private final static QName _SaveToXmlJaxb_QNAME = new QName("http://endpoint.api.tm.fadeev.ru/", "saveToXmlJaxb");
    private final static QName _SaveToXmlJaxbResponse_QNAME = new QName("http://endpoint.api.tm.fadeev.ru/", "saveToXmlJaxbResponse");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: ru.fadeev.tm.api.endpoint
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link Exception }
     * 
     */
    public Exception createException() {
        return new Exception();
    }

    /**
     * Create an instance of {@link LoadFromBinary }
     * 
     */
    public LoadFromBinary createLoadFromBinary() {
        return new LoadFromBinary();
    }

    /**
     * Create an instance of {@link LoadFromBinaryResponse }
     * 
     */
    public LoadFromBinaryResponse createLoadFromBinaryResponse() {
        return new LoadFromBinaryResponse();
    }

    /**
     * Create an instance of {@link LoadFromJsonFasterXml }
     * 
     */
    public LoadFromJsonFasterXml createLoadFromJsonFasterXml() {
        return new LoadFromJsonFasterXml();
    }

    /**
     * Create an instance of {@link LoadFromJsonFasterXmlResponse }
     * 
     */
    public LoadFromJsonFasterXmlResponse createLoadFromJsonFasterXmlResponse() {
        return new LoadFromJsonFasterXmlResponse();
    }

    /**
     * Create an instance of {@link LoadFromJsonJaxb }
     * 
     */
    public LoadFromJsonJaxb createLoadFromJsonJaxb() {
        return new LoadFromJsonJaxb();
    }

    /**
     * Create an instance of {@link LoadFromJsonJaxbResponse }
     * 
     */
    public LoadFromJsonJaxbResponse createLoadFromJsonJaxbResponse() {
        return new LoadFromJsonJaxbResponse();
    }

    /**
     * Create an instance of {@link LoadFromXmlFasterxml }
     * 
     */
    public LoadFromXmlFasterxml createLoadFromXmlFasterxml() {
        return new LoadFromXmlFasterxml();
    }

    /**
     * Create an instance of {@link LoadFromXmlFasterxmlResponse }
     * 
     */
    public LoadFromXmlFasterxmlResponse createLoadFromXmlFasterxmlResponse() {
        return new LoadFromXmlFasterxmlResponse();
    }

    /**
     * Create an instance of {@link LoadFromXmlJaxb }
     * 
     */
    public LoadFromXmlJaxb createLoadFromXmlJaxb() {
        return new LoadFromXmlJaxb();
    }

    /**
     * Create an instance of {@link LoadFromXmlJaxbResponse }
     * 
     */
    public LoadFromXmlJaxbResponse createLoadFromXmlJaxbResponse() {
        return new LoadFromXmlJaxbResponse();
    }

    /**
     * Create an instance of {@link SaveToBinary }
     * 
     */
    public SaveToBinary createSaveToBinary() {
        return new SaveToBinary();
    }

    /**
     * Create an instance of {@link SaveToBinaryResponse }
     * 
     */
    public SaveToBinaryResponse createSaveToBinaryResponse() {
        return new SaveToBinaryResponse();
    }

    /**
     * Create an instance of {@link SaveToJsonFasterXml }
     * 
     */
    public SaveToJsonFasterXml createSaveToJsonFasterXml() {
        return new SaveToJsonFasterXml();
    }

    /**
     * Create an instance of {@link SaveToJsonFasterXmlResponse }
     * 
     */
    public SaveToJsonFasterXmlResponse createSaveToJsonFasterXmlResponse() {
        return new SaveToJsonFasterXmlResponse();
    }

    /**
     * Create an instance of {@link SaveToJsonJaxb }
     * 
     */
    public SaveToJsonJaxb createSaveToJsonJaxb() {
        return new SaveToJsonJaxb();
    }

    /**
     * Create an instance of {@link SaveToJsonJaxbResponse }
     * 
     */
    public SaveToJsonJaxbResponse createSaveToJsonJaxbResponse() {
        return new SaveToJsonJaxbResponse();
    }

    /**
     * Create an instance of {@link SaveToXmlFasterxml }
     * 
     */
    public SaveToXmlFasterxml createSaveToXmlFasterxml() {
        return new SaveToXmlFasterxml();
    }

    /**
     * Create an instance of {@link SaveToXmlFasterxmlResponse }
     * 
     */
    public SaveToXmlFasterxmlResponse createSaveToXmlFasterxmlResponse() {
        return new SaveToXmlFasterxmlResponse();
    }

    /**
     * Create an instance of {@link SaveToXmlJaxb }
     * 
     */
    public SaveToXmlJaxb createSaveToXmlJaxb() {
        return new SaveToXmlJaxb();
    }

    /**
     * Create an instance of {@link SaveToXmlJaxbResponse }
     * 
     */
    public SaveToXmlJaxbResponse createSaveToXmlJaxbResponse() {
        return new SaveToXmlJaxbResponse();
    }

    /**
     * Create an instance of {@link Session }
     * 
     */
    public Session createSession() {
        return new Session();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Exception }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://endpoint.api.tm.fadeev.ru/", name = "Exception")
    public JAXBElement<Exception> createException(Exception value) {
        return new JAXBElement<Exception>(_Exception_QNAME, Exception.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link LoadFromBinary }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://endpoint.api.tm.fadeev.ru/", name = "loadFromBinary")
    public JAXBElement<LoadFromBinary> createLoadFromBinary(LoadFromBinary value) {
        return new JAXBElement<LoadFromBinary>(_LoadFromBinary_QNAME, LoadFromBinary.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link LoadFromBinaryResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://endpoint.api.tm.fadeev.ru/", name = "loadFromBinaryResponse")
    public JAXBElement<LoadFromBinaryResponse> createLoadFromBinaryResponse(LoadFromBinaryResponse value) {
        return new JAXBElement<LoadFromBinaryResponse>(_LoadFromBinaryResponse_QNAME, LoadFromBinaryResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link LoadFromJsonFasterXml }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://endpoint.api.tm.fadeev.ru/", name = "loadFromJsonFasterXml")
    public JAXBElement<LoadFromJsonFasterXml> createLoadFromJsonFasterXml(LoadFromJsonFasterXml value) {
        return new JAXBElement<LoadFromJsonFasterXml>(_LoadFromJsonFasterXml_QNAME, LoadFromJsonFasterXml.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link LoadFromJsonFasterXmlResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://endpoint.api.tm.fadeev.ru/", name = "loadFromJsonFasterXmlResponse")
    public JAXBElement<LoadFromJsonFasterXmlResponse> createLoadFromJsonFasterXmlResponse(LoadFromJsonFasterXmlResponse value) {
        return new JAXBElement<LoadFromJsonFasterXmlResponse>(_LoadFromJsonFasterXmlResponse_QNAME, LoadFromJsonFasterXmlResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link LoadFromJsonJaxb }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://endpoint.api.tm.fadeev.ru/", name = "loadFromJsonJaxb")
    public JAXBElement<LoadFromJsonJaxb> createLoadFromJsonJaxb(LoadFromJsonJaxb value) {
        return new JAXBElement<LoadFromJsonJaxb>(_LoadFromJsonJaxb_QNAME, LoadFromJsonJaxb.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link LoadFromJsonJaxbResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://endpoint.api.tm.fadeev.ru/", name = "loadFromJsonJaxbResponse")
    public JAXBElement<LoadFromJsonJaxbResponse> createLoadFromJsonJaxbResponse(LoadFromJsonJaxbResponse value) {
        return new JAXBElement<LoadFromJsonJaxbResponse>(_LoadFromJsonJaxbResponse_QNAME, LoadFromJsonJaxbResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link LoadFromXmlFasterxml }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://endpoint.api.tm.fadeev.ru/", name = "loadFromXmlFasterxml")
    public JAXBElement<LoadFromXmlFasterxml> createLoadFromXmlFasterxml(LoadFromXmlFasterxml value) {
        return new JAXBElement<LoadFromXmlFasterxml>(_LoadFromXmlFasterxml_QNAME, LoadFromXmlFasterxml.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link LoadFromXmlFasterxmlResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://endpoint.api.tm.fadeev.ru/", name = "loadFromXmlFasterxmlResponse")
    public JAXBElement<LoadFromXmlFasterxmlResponse> createLoadFromXmlFasterxmlResponse(LoadFromXmlFasterxmlResponse value) {
        return new JAXBElement<LoadFromXmlFasterxmlResponse>(_LoadFromXmlFasterxmlResponse_QNAME, LoadFromXmlFasterxmlResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link LoadFromXmlJaxb }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://endpoint.api.tm.fadeev.ru/", name = "loadFromXmlJaxb")
    public JAXBElement<LoadFromXmlJaxb> createLoadFromXmlJaxb(LoadFromXmlJaxb value) {
        return new JAXBElement<LoadFromXmlJaxb>(_LoadFromXmlJaxb_QNAME, LoadFromXmlJaxb.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link LoadFromXmlJaxbResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://endpoint.api.tm.fadeev.ru/", name = "loadFromXmlJaxbResponse")
    public JAXBElement<LoadFromXmlJaxbResponse> createLoadFromXmlJaxbResponse(LoadFromXmlJaxbResponse value) {
        return new JAXBElement<LoadFromXmlJaxbResponse>(_LoadFromXmlJaxbResponse_QNAME, LoadFromXmlJaxbResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SaveToBinary }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://endpoint.api.tm.fadeev.ru/", name = "saveToBinary")
    public JAXBElement<SaveToBinary> createSaveToBinary(SaveToBinary value) {
        return new JAXBElement<SaveToBinary>(_SaveToBinary_QNAME, SaveToBinary.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SaveToBinaryResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://endpoint.api.tm.fadeev.ru/", name = "saveToBinaryResponse")
    public JAXBElement<SaveToBinaryResponse> createSaveToBinaryResponse(SaveToBinaryResponse value) {
        return new JAXBElement<SaveToBinaryResponse>(_SaveToBinaryResponse_QNAME, SaveToBinaryResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SaveToJsonFasterXml }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://endpoint.api.tm.fadeev.ru/", name = "saveToJsonFasterXml")
    public JAXBElement<SaveToJsonFasterXml> createSaveToJsonFasterXml(SaveToJsonFasterXml value) {
        return new JAXBElement<SaveToJsonFasterXml>(_SaveToJsonFasterXml_QNAME, SaveToJsonFasterXml.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SaveToJsonFasterXmlResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://endpoint.api.tm.fadeev.ru/", name = "saveToJsonFasterXmlResponse")
    public JAXBElement<SaveToJsonFasterXmlResponse> createSaveToJsonFasterXmlResponse(SaveToJsonFasterXmlResponse value) {
        return new JAXBElement<SaveToJsonFasterXmlResponse>(_SaveToJsonFasterXmlResponse_QNAME, SaveToJsonFasterXmlResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SaveToJsonJaxb }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://endpoint.api.tm.fadeev.ru/", name = "saveToJsonJaxb")
    public JAXBElement<SaveToJsonJaxb> createSaveToJsonJaxb(SaveToJsonJaxb value) {
        return new JAXBElement<SaveToJsonJaxb>(_SaveToJsonJaxb_QNAME, SaveToJsonJaxb.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SaveToJsonJaxbResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://endpoint.api.tm.fadeev.ru/", name = "saveToJsonJaxbResponse")
    public JAXBElement<SaveToJsonJaxbResponse> createSaveToJsonJaxbResponse(SaveToJsonJaxbResponse value) {
        return new JAXBElement<SaveToJsonJaxbResponse>(_SaveToJsonJaxbResponse_QNAME, SaveToJsonJaxbResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SaveToXmlFasterxml }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://endpoint.api.tm.fadeev.ru/", name = "saveToXmlFasterxml")
    public JAXBElement<SaveToXmlFasterxml> createSaveToXmlFasterxml(SaveToXmlFasterxml value) {
        return new JAXBElement<SaveToXmlFasterxml>(_SaveToXmlFasterxml_QNAME, SaveToXmlFasterxml.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SaveToXmlFasterxmlResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://endpoint.api.tm.fadeev.ru/", name = "saveToXmlFasterxmlResponse")
    public JAXBElement<SaveToXmlFasterxmlResponse> createSaveToXmlFasterxmlResponse(SaveToXmlFasterxmlResponse value) {
        return new JAXBElement<SaveToXmlFasterxmlResponse>(_SaveToXmlFasterxmlResponse_QNAME, SaveToXmlFasterxmlResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SaveToXmlJaxb }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://endpoint.api.tm.fadeev.ru/", name = "saveToXmlJaxb")
    public JAXBElement<SaveToXmlJaxb> createSaveToXmlJaxb(SaveToXmlJaxb value) {
        return new JAXBElement<SaveToXmlJaxb>(_SaveToXmlJaxb_QNAME, SaveToXmlJaxb.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SaveToXmlJaxbResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://endpoint.api.tm.fadeev.ru/", name = "saveToXmlJaxbResponse")
    public JAXBElement<SaveToXmlJaxbResponse> createSaveToXmlJaxbResponse(SaveToXmlJaxbResponse value) {
        return new JAXBElement<SaveToXmlJaxbResponse>(_SaveToXmlJaxbResponse_QNAME, SaveToXmlJaxbResponse.class, null, value);
    }

}
