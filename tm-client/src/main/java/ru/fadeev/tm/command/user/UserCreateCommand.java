package ru.fadeev.tm.command.user;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.fadeev.tm.api.endpoint.*;
import ru.fadeev.tm.api.service.ITerminalService;
import ru.fadeev.tm.command.AbstractCommand;


import java.lang.Exception;

public final class UserCreateCommand extends AbstractCommand {

    @Override
    public boolean permission(Role role) {
        return true;
    }

    @NotNull
    @Override
    public String getName() {
        return "user-create";
    }

    @NotNull
    @Override
    public String getDescription() {
        return "creates a new user account";
    }

    @Override
    public void execute() throws Exception {
        @NotNull final ITerminalService terminal = serviceLocator.getTerminalService();
        @Nullable final Session session = serviceLocator.getAppStateService().getSession();
        @Nullable final IUserEndpoint userEndpoint = serviceLocator.getUserEndpoint();
        @Nullable final boolean isAdministrator = session != null && Role.ADMINISTRATOR == session.getRole();
        terminal.println("[CREATE ACCOUNT]");
        terminal.println("ENTER NAME");
        @Nullable final String login = terminal.readString();
        @NotNull boolean isLoginExist = userEndpoint.isLoginExistUser(login);
        if (login == null || login.isEmpty()) throw new Exception("Incorrect name");
        if (isLoginExist) throw new Exception("User with same name already exist");
        @NotNull User user = new User();
        user.setLogin(login);
        terminal.println("Enter password:");
        user.setPasswordHash(terminal.readString());
        if (!isAdministrator){
            userEndpoint.persistUser(user);
            terminal.print("[OK]\n");
            return;
        }
        setRole(session, user);
        terminal.println("[OK]\n");
    }

    public void setRole(@NotNull final Session session, @NotNull User user) throws Exception_Exception {
        @NotNull final ITerminalService terminal = serviceLocator.getTerminalService();
        terminal.println("would you like give new account administrator rights? y/n");
        if ("y".equals(terminal.readString())) serviceLocator.getUserEndpoint().setAdminRole(session, user);
        else serviceLocator.getUserEndpoint().persistUser(user);
    }

}