package ru.fadeev.tm.command.data.json;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.fadeev.tm.api.endpoint.Role;
import ru.fadeev.tm.api.endpoint.Session;
import ru.fadeev.tm.api.service.ITerminalService;
import ru.fadeev.tm.command.AbstractCommand;

import java.nio.file.AccessDeniedException;

public final class DataJsonJaxbLoadFile extends AbstractCommand {

    @NotNull
    @Override
    public String getName() {
        return "data-json-jaxb-load";
    }

    @NotNull
    @Override
    public String getDescription() {
        return "load data form json format file (JAXB)";
    }

    @Override
    public void execute() throws Exception{
        @NotNull final ITerminalService terminal = serviceLocator.getTerminalService();
        @Nullable final Session session = serviceLocator.getAppStateService().getSession();
        if (session == null) throw new AccessDeniedException("Access Denied");
        terminal.println("[LOAD DATA FROM JSON FORMAT FILE (JAXB)]");
        serviceLocator.getDomainEndpoint().loadFromJsonJaxb(session);
        serviceLocator.getAppStateService().setSession(null);
        terminal.println("You will be logged out of the application");
        terminal.println("[OK]\n");
    }

    @Override
    public @Nullable Role[] accessRole() {
        return new Role[]{Role.ADMINISTRATOR};
    }

}