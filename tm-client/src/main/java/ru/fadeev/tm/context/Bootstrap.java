package ru.fadeev.tm.context;

import lombok.Getter;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.reflections.Reflections;
import ru.fadeev.tm.api.endpoint.*;
import ru.fadeev.tm.api.repository.IAppStateRepository;
import ru.fadeev.tm.api.service.IAppStateService;
import ru.fadeev.tm.api.service.ITerminalService;
import ru.fadeev.tm.api.service.ServiceLocator;
import ru.fadeev.tm.command.AbstractCommand;
import ru.fadeev.tm.endpoint.*;
import ru.fadeev.tm.exception.AccessDeniedException;
import ru.fadeev.tm.exception.CommandCorruptException;
import ru.fadeev.tm.exception.IllegalCommandNameException;
import ru.fadeev.tm.repository.AppStateRepository;
import ru.fadeev.tm.service.AppStateService;
import ru.fadeev.tm.service.TerminalService;

import java.lang.Exception;
import java.util.Arrays;
import java.util.Set;

@Getter
@Setter
public class Bootstrap implements ServiceLocator {

    @NotNull
    IProjectEndpoint projectEndpoint = new ProjectEndpointService().getProjectEndpointPort();

    @NotNull
    ISessionEndpoint sessionEndpoint = new SessionEndpointService().getSessionEndpointPort();

    @NotNull
    ITaskEndpoint taskEndpoint = new TaskEndpointService().getTaskEndpointPort();

    @NotNull
    IUserEndpoint userEndpoint = new UserEndpointService().getUserEndpointPort();

    @NotNull
    IDomainEndpoint domainEndpoint = new DomainEndpointService().getDomainEndpointPort();

    @NotNull
    IAppStateRepository appStateRepository = new AppStateRepository();

    @NotNull
    IAppStateService appStateService = new AppStateService(appStateRepository);

    ITerminalService terminalService = new TerminalService();

    public void init() {
        initCommand();
        start();
    }

    private void initCommand() {
        final Set<Class<? extends AbstractCommand>> classes =
                new Reflections("ru.fadeev.tm").getSubTypesOf(AbstractCommand.class);
        for (Class<? extends AbstractCommand> commandClass : classes) {
            try {
                registry(commandClass.newInstance());
            } catch (InstantiationException | IllegalAccessException e) {
                throw new CommandCorruptException();
            }
        }
    }

    private void start() {
        terminalService.println("*** WELCOME TO TASK MANAGER ***");
        @Nullable String command = "";
        while (!"exit".equals(command)) {
            try {
                command = getTerminalService().readString();
                execute(command);
            } catch (final Exception e) {
                terminalService.println(e.getMessage());
            }
        }
    }

    private void registry(@NotNull final AbstractCommand command) {
        @Nullable final String cliCommand = command.getName();
        @Nullable final String cliDescription = command.getDescription();
        if (cliCommand.isEmpty()) throw new CommandCorruptException();
        if (cliDescription.isEmpty()) throw new CommandCorruptException();
        command.setServiceLocator(this);
        appStateService.putCommand(cliCommand, command);
    }

    private void execute(@Nullable final String command) throws Exception {
        if (command == null || command.isEmpty()) return;
        @Nullable final AbstractCommand abstractCommand = getAppStateService().getCommand(command);
        if (abstractCommand == null) throw new IllegalCommandNameException("Wrong command name");
        if (!checkPermission(abstractCommand)) throw new AccessDeniedException("Access denied");
        abstractCommand.execute();
    }

    private boolean checkPermission(@NotNull final AbstractCommand abstractCommand) {
        @Nullable final Role role = getAppStateService().getRole();
        final boolean permission = abstractCommand.permission(role);
        @Nullable final Role[] accessRoles = abstractCommand.accessRole();
        if (accessRoles == null) return permission;
        if (role == null) return false;
        final boolean rolePermission = Arrays.asList(accessRoles).contains(role);
        return permission && rolePermission;
    }

}