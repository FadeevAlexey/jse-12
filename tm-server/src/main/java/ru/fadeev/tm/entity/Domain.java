package ru.fadeev.tm.entity;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import org.jetbrains.annotations.Nullable;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import java.io.Serializable;
import java.util.List;

@Getter
@Setter
@ToString
@JsonAutoDetect
@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
public class Domain implements Serializable {

    private static final long serialVersionUID = 6852833094660198445L;

    @Nullable
    @XmlElement
    private List<User> users;

    @Nullable
    @XmlElement
    private List<Task> tasks;

    @Nullable
    @XmlElement
    private List<Project> projects;

}