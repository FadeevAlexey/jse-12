package ru.fadeev.tm.repository;

import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.fadeev.tm.api.repository.ISessionRepository;
import ru.fadeev.tm.constant.FiledConstant;
import ru.fadeev.tm.entity.Session;
import ru.fadeev.tm.enumerated.Role;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

@Setter
@NoArgsConstructor
public class SessionRepository extends AbstractRepository<Session> implements ISessionRepository {

    @NotNull
    private Connection connection;

    public SessionRepository(@NotNull final Connection connection) {
        this.connection = connection;
    }

    @Override
    public @NotNull List<Session> findAll() throws SQLException {
        @NotNull final String query = "SELECT * FROM app_session";
        @NotNull final PreparedStatement statement = connection.prepareStatement(query);
        @NotNull final ResultSet resultSet = statement.executeQuery();
        if (resultSet == null) return Collections.emptyList();
        @NotNull final List<Session> sessions = new ArrayList<>();
        while (resultSet.next()) sessions.add(fetch(resultSet));
        statement.close();
        resultSet.close();
        return sessions;
    }

    @Nullable
    @Override
    public Session findOne(@NotNull final String id) throws SQLException {
        @NotNull final String query = "SELECT * FROM app_session WHERE id = ?";
        @NotNull final PreparedStatement statement = connection.prepareStatement(query);
        statement.setString(1, id);
        @NotNull final ResultSet resultSet = statement.executeQuery();
        if (!resultSet.next()) return null;
        @Nullable final Session session = fetch(resultSet);
        resultSet.close();
        statement.close();
        return session;
    }

    @Override
    public void remove(@NotNull final String id) throws SQLException {
        @NotNull final String query = "DELETE FROM app_session WHERE id = ?";
        @NotNull final PreparedStatement statement = connection.prepareStatement(query);
        statement.setString(1, id);
        statement.executeUpdate();
        statement.close();
        connection.commit();
    }

    @Override
    public void persist(@NotNull final Session session) throws Exception {
        @NotNull final String query = "INSERT INTO app_session VALUES (?, ?, ?, ?, ?)";
        @NotNull final PreparedStatement statement = connection.prepareStatement(query);
        statement.setString(1, session.getId());
        statement.setString(2, session.getUserId());
        statement.setString(3, session.getSignature());
        statement.setLong(4,session.getCreationTime());
        statement.setString(5, session.getRole().name());
        statement.executeUpdate();
        connection.commit();
        statement.close();
    }

    @Override
    public void merge(@NotNull final Session session) throws Exception {
        @NotNull final String query =
                "UPDATE app_session " +
                "SET userId = ?, signature = ?, creationTime = ?, role = ?   " +
                "WHERE id = ?";
        @NotNull final PreparedStatement statement = connection.prepareStatement(query);
        statement.setString(1, session.getUserId());
        statement.setString(2, session.getSignature());
        statement.setLong(3, session.getCreationTime());
        statement.setString(4, session.getRole().name());
        statement.setString(5, session.getId());
        statement.executeUpdate();
        statement.close();
        connection.commit();
    }

    @Override
    public void removeAll() throws SQLException {
        @NotNull final String query = "DELETE FROM app_session";
        @NotNull final PreparedStatement statement = connection.prepareStatement(query);
        statement.executeUpdate();
        statement.close();
        connection.commit();
    }

    @Nullable
    @Override
    public Session findByUserId(@NotNull final String userId) throws SQLException {
        @NotNull final String query = "SELECT * FROM app_session WHERE userId = ?";
        @NotNull final PreparedStatement statement = connection.prepareStatement(query);
        statement.setString(1, userId);
        @NotNull final ResultSet resultSet = statement.executeQuery();
        if (!resultSet.next()) return null;
        @Nullable final Session session = fetch(resultSet);
        resultSet.close();
        statement.close();
        return session;
    }

    @Override
    public void removeSessionBySignature(@NotNull final String signature) throws SQLException {
        @NotNull final String query = "DELETE FROM app_session WHERE signature = ?";
        @NotNull final PreparedStatement statement = connection.prepareStatement(query);
        statement.setString(1,signature);
        statement.executeUpdate();
        statement.close();
        connection.commit();
    }

    public boolean contains(@NotNull final String sessionId) throws SQLException {
        @NotNull final String query = "SELECT EXISTS (SELECT id from app_session WHERE id = ?)";
        @NotNull final PreparedStatement statement = connection.prepareStatement(query);
        statement.setString(1, sessionId);
        @NotNull final ResultSet resultSet = statement.executeQuery();
        if (!resultSet.next()) return false;
        @NotNull boolean result = resultSet.getInt(1) >= 1;
        statement.close();
        return result;
    }


    @Nullable
    private Session fetch(@Nullable final ResultSet row) throws SQLException {
        if (row == null) return null;
        @NotNull final Session session = new Session();
        session.setId(row.getString(FiledConstant.ID));
        session.setUserId(row.getString(FiledConstant.USER_ID));
        session.setSignature(row.getString(FiledConstant.SIGNATURE));
        session.setCreationTime(row.getLong(FiledConstant.CREATION_TIME));
        session.setRole(Role.valueOf(row.getString(FiledConstant.ROLE)));
        return session;
    }

}