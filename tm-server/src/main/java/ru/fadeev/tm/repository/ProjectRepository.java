package ru.fadeev.tm.repository;

import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.fadeev.tm.api.repository.IProjectRepository;
import ru.fadeev.tm.entity.Project;
import ru.fadeev.tm.enumerated.Status;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.*;

import static ru.fadeev.tm.util.DbUtil.getSqlDate;
import static ru.fadeev.tm.constant.FiledConstant.*;

@Setter
@NoArgsConstructor
public final class ProjectRepository extends AbstractRepository<Project> implements IProjectRepository {

    @NotNull
    private Connection connection;

    public ProjectRepository(@NotNull final Connection connection) {
        this.connection = connection;
    }

    @Override
    public @NotNull List<Project> findAll() throws SQLException {
        @NotNull final String query = "SELECT * FROM app_project";
        @NotNull final PreparedStatement statement = connection.prepareStatement(query);
        @NotNull final ResultSet resultSet = statement.executeQuery();
        if (resultSet == null) return Collections.emptyList();
        @NotNull final List<Project> projects = new ArrayList<>();
        while (resultSet.next()) projects.add(fetch(resultSet));
        statement.close();
        resultSet.close();
        return projects;
    }

    @Nullable
    @Override
    public Project findOne(@NotNull final String id) throws SQLException {
        @NotNull final String query = "SELECT * FROM app_project WHERE id = ?";
        @NotNull final PreparedStatement statement = connection.prepareStatement(query);
        statement.setString(1, id);
        @NotNull final ResultSet resultSet = statement.executeQuery();
        if (!resultSet.next()) return null;
        @Nullable final Project project = fetch(resultSet);
        resultSet.close();
        statement.close();
        return project;
    }

    @Nullable
    @Override
    public Project findOne(@NotNull final String userId, @NotNull final String id) throws SQLException {
        @NotNull final String query = "SELECT * FROM app_project WHERE userId = ? and id = ?";
        @NotNull final PreparedStatement statement = connection.prepareStatement(query);
        statement.setString(1, userId);
        statement.setString(2, id);
        @NotNull final ResultSet resultSet = statement.executeQuery();
        if (!resultSet.next()) return null;
        @Nullable final Project project = fetch(resultSet);
        resultSet.close();
        statement.close();
        return project;
    }

    @Override
    public void remove(@NotNull final String id) throws SQLException {
        @NotNull final String query = "DELETE FROM app_project WHERE id = ?";
        @NotNull final PreparedStatement statement = connection.prepareStatement(query);
        statement.setString(1, id);
        statement.executeUpdate();
        statement.close();
        connection.commit();
    }

    @Override
    public void remove(@NotNull final String userId, @NotNull final String projectId) throws SQLException {
        @NotNull final String query = "DELETE FROM app_project WHERE userId = ? AND id = ?";
        @NotNull final PreparedStatement statement = connection.prepareStatement(query);
        statement.setString(1, userId);
        statement.setString(2, projectId);
        statement.executeUpdate();
        statement.close();
        connection.commit();
    }

    @Override
    public void persist(@NotNull final Project project) throws Exception {
        @NotNull final String query = "INSERT INTO app_project VALUES (?, ?, ?, ?, ?, ?, ?,?)";
        @NotNull final PreparedStatement statement = connection.prepareStatement(query);
        statement.setString(1, project.getId());
        statement.setString(2, project.getName());
        statement.setString(3, project.getDescription());
        statement.setDate(4, getSqlDate(project.getStartDate()));
        statement.setDate(5, getSqlDate(project.getFinishDate()));
        statement.setString(6, project.getUserId());
        statement.setString(7, project.getStatus().name());
        statement.setDate(8, getSqlDate(project.getCreateDate()));
        statement.executeUpdate();
        connection.commit();
        statement.close();
    }

    @Override
    public void merge(@NotNull final Project project) throws Exception {
        @NotNull final String query =
                "UPDATE app_project " +
                        "SET name = ?, description = ?, startDate = ?, finishDate = ?, userId = ?, status = ?, creationTime = ?" +
                        "WHERE id = ?";
        @NotNull final PreparedStatement statement = connection.prepareStatement(query);
        statement.setString(1, project.getName());
        statement.setString(2, project.getDescription());
        statement.setDate(3, getSqlDate(project.getStartDate()));
        statement.setDate(4, getSqlDate(project.getFinishDate()));
        statement.setString(5, project.getUserId());
        statement.setString(6, project.getStatus().name());
        statement.setDate(7, getSqlDate(project.getCreateDate()));
        statement.setString(8, project.getId());
        statement.executeUpdate();
        statement.close();
        connection.commit();
    }

    @Override
    public void removeAll() throws SQLException {
        @NotNull final String query = "DELETE FROM app_project";
        @NotNull final PreparedStatement statement = connection.prepareStatement(query);
        statement.executeUpdate();
        statement.close();
        connection.commit();
    }

    @Override
    public void removeAll(@NotNull final String userId) throws SQLException {
        @NotNull final String query = "DELETE FROM app_project WHERE userId = ?";
        @NotNull final PreparedStatement statement = connection.prepareStatement(query);
        statement.setString(1, userId);
        statement.executeUpdate();
        statement.close();
        connection.commit();
    }

    @NotNull
    @Override
    public List<Project> findAll(@NotNull final String userId) throws SQLException {
        @NotNull final String query = "SELECT * FROM app_project WHERE userId = ?";
        @NotNull final PreparedStatement statement = connection.prepareStatement(query);
        statement.setString(1, userId);
        @NotNull final ResultSet resultSet = statement.executeQuery();
        if (resultSet == null) return Collections.emptyList();
        @NotNull final List<Project> projectList = new ArrayList<>();
        while (resultSet.next()) projectList.add(fetch(resultSet));
        statement.close();
        resultSet.close();
        return projectList;
    }

    @Nullable
    @Override
    public String findIdByName(@NotNull final String userId, @NotNull final String name) throws SQLException {
        @NotNull final String query = "SELECT id FROM app_project WHERE userId = ? and name = ?";
        @NotNull final PreparedStatement statement = connection.prepareStatement(query);
        statement.setString(1, userId);
        statement.setString(2, name);
        @NotNull final ResultSet resultSet = statement.executeQuery();
        if (resultSet == null) return null;
        resultSet.next();
        @NotNull final String id = resultSet.getString(ID);
        statement.close();
        resultSet.close();
        return id;
    }

    @NotNull
    public Collection<Project> searchByName(@NotNull final String userId, @NotNull final String name) throws SQLException {
        @NotNull final String query = "SELECT * FROM app_project WHERE userId = ? AND name LIKE CONCAT('%',?,'%')";
        @NotNull final PreparedStatement statement = connection.prepareStatement(query);
        statement.setString(1, userId);
        statement.setString(2, name);
        @NotNull final ResultSet resultSet = statement.executeQuery();
        if (resultSet == null) return Collections.emptyList();
        @NotNull final List<Project> projectList = new ArrayList<>();
        while (resultSet.next()) projectList.add(fetch(resultSet));
        statement.close();
        resultSet.close();
        return projectList;
    }

    @NotNull
    public Collection<Project> searchByDescription(@NotNull final String userId, @NotNull final String description) throws SQLException {
        @NotNull final String query = "SELECT * FROM app_project WHERE userId = ? AND description LIKE CONCAT('%',?,'%')";
        @NotNull final PreparedStatement statement = connection.prepareStatement(query);
        statement.setString(1, userId);
        statement.setString(2, description);
        @NotNull final ResultSet resultSet = statement.executeQuery();
        if (resultSet == null) return Collections.emptyList();
        @NotNull final List<Project> projectList = new ArrayList<>();
        while (resultSet.next()) projectList.add(fetch(resultSet));
        statement.close();
        resultSet.close();
        return projectList;
    }

    @NotNull
    public Collection<Project> sortAllByStartDate(@NotNull final String userId, @NotNull final String sortType) throws SQLException {
        @NotNull String query = "SELECT * FROM app_project WHERE userId = ? ORDER BY startDate";
        if ("desc".equals(sortType.toLowerCase()))
            query = "SELECT * FROM app_project WHERE userId = ? ORDER BY startDate DESC";
        @NotNull final PreparedStatement statement = connection.prepareStatement(query);
        statement.setString(1, userId);
        @NotNull final ResultSet resultSet = statement.executeQuery();
        if (resultSet == null) return Collections.emptyList();
        @NotNull final List<Project> projects = new ArrayList<>();
        while (resultSet.next()) projects.add(fetch(resultSet));
        statement.close();
        resultSet.close();
        return projects;
    }

    @NotNull
    public Collection<Project> sortAllByFinishDate(@NotNull final String userId, @NotNull final String sortType) throws SQLException {
        @NotNull String query = "SELECT * FROM app_project WHERE userId = ? ORDER BY finishDate";
        if ("desc".equals(sortType.toLowerCase()))
            query = "SELECT * FROM app_project WHERE userId = ? ORDER BY finishDate DESC";
        @NotNull final PreparedStatement statement = connection.prepareStatement(query);
        statement.setString(1, userId);
        @NotNull final ResultSet resultSet = statement.executeQuery();
        if (resultSet == null) return Collections.emptyList();
        @NotNull final List<Project> projects = new ArrayList<>();
        while (resultSet.next()) projects.add(fetch(resultSet));
        statement.close();
        resultSet.close();
        return projects;
    }

    @NotNull
    public Collection<Project> sortAllByStatus(@NotNull final String userId, @NotNull final String sortType) throws SQLException {
        @NotNull String query = "SELECT * FROM app_project WHERE userId = ? ORDER BY status";
        if ("desc".equals(sortType.toLowerCase()))
            query = "SELECT * FROM app_project WHERE userId = ? ORDER BY status DESC";
        @NotNull final PreparedStatement statement = connection.prepareStatement(query);
        statement.setString(1, userId);
        @NotNull final ResultSet resultSet = statement.executeQuery();
        if (resultSet == null) return Collections.emptyList();
        @NotNull final List<Project> projects = new ArrayList<>();
        while (resultSet.next()) projects.add(fetch(resultSet));
        statement.close();
        resultSet.close();
        return projects;
    }

    @NotNull
    public Collection<Project> sortAllByCreationDate(@NotNull final String userId) throws SQLException {
        @NotNull String query = "SELECT * FROM app_project WHERE userId = ? ORDER BY status";
        @NotNull final PreparedStatement statement = connection.prepareStatement(query);
        statement.setString(1, userId);
        @NotNull final ResultSet resultSet = statement.executeQuery();
        if (resultSet == null) return Collections.emptyList();
        @NotNull final List<Project> projects = new ArrayList<>();
        while (resultSet.next()) projects.add(fetch(resultSet));
        statement.close();
        resultSet.close();
        return projects;
    }


    @Nullable
    private Project fetch(@Nullable final ResultSet row) throws SQLException {
        if (row == null) return null;
        @NotNull final Project project = new Project();
        project.setId(row.getString(ID));
        project.setName(row.getString(NAME));
        project.setDescription(row.getString(DESCRIPTION));
        project.setStartDate(row.getDate(START_DATE));
        project.setFinishDate(row.getDate(FINISH_DATE));
        project.setUserId(row.getString(USER_ID));
        project.setStatus(Status.valueOf(row.getString(STATUS)));
        project.setCreateDate(row.getDate(CREATE_DATE));
        return project;
    }

}