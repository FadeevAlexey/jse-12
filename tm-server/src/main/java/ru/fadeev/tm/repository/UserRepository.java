package ru.fadeev.tm.repository;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.fadeev.tm.api.repository.IUserRepository;
import ru.fadeev.tm.constant.FiledConstant;
import ru.fadeev.tm.entity.User;
import ru.fadeev.tm.enumerated.Role;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

@Setter
@Getter
@NoArgsConstructor
public final class UserRepository extends AbstractRepository<User> implements IUserRepository {

    @NotNull
    private Connection connection;

    public UserRepository(@NotNull final Connection connection) {
        this.connection = connection;
    }

    @Override
    public @NotNull List<User> findAll() throws SQLException {
        @NotNull final String query = "SELECT * FROM app_user";
        @NotNull final PreparedStatement statement = connection.prepareStatement(query);
        @NotNull final ResultSet resultSet = statement.executeQuery();
        if (resultSet == null) return Collections.emptyList();
        List<User> users = new ArrayList<>();
        while (resultSet.next()) users.add(fetch(resultSet));
        statement.close();
        resultSet.close();
        return users;
    }

    public void remove(@NotNull final String id) throws SQLException {
        @NotNull final String query = "DELETE FROM app_user WHERE id = ?";
        @NotNull final PreparedStatement statement = connection.prepareStatement(query);
        statement.setString(1, id);
        statement.executeUpdate();
        statement.close();
        connection.commit();
    }

    @Override
    public void merge(@NotNull final User user) throws SQLException {
        @NotNull final String query = "UPDATE app_user SET login = ?, passwordHash = ?, role = ?  WHERE id = ?";
        @NotNull final PreparedStatement statement = connection.prepareStatement(query);
        statement.setString(1, user.getLogin());
        statement.setString(2, user.getPasswordHash());
        statement.setString(3, user.getRole().name());
        statement.setString(4, user.getId());
        statement.executeUpdate();
        statement.close();
        connection.commit();
    }

    @Override
    public void removeAll() throws SQLException {
        @NotNull final String query = "DELETE FROM app_user";
        @NotNull final PreparedStatement statement = connection.prepareStatement(query);
        statement.executeUpdate();
        statement.close();
        connection.commit();
    }

    @Nullable
    @Override
    public User findOne(@NotNull final String id) throws SQLException {
        @NotNull final String query = "SELECT * FROM app_user WHERE id = ?";
        @NotNull final PreparedStatement statement = connection.prepareStatement(query);
        statement.setString(1, id);
        @NotNull final ResultSet resultSet = statement.executeQuery();
        if (!resultSet.next()) return null;
        @Nullable final User user = fetch(resultSet);
        resultSet.close();
        statement.close();
        return user;
    }

    @Override
    public void persist(@NotNull final User user) throws SQLException {
        @NotNull final String query = "INSERT INTO app_user VALUES (?, ?, ?, ?)";
        @NotNull final PreparedStatement statement = connection.prepareStatement(query);
        statement.setString(1, user.getId());
        statement.setString(2, user.getLogin());
        statement.setString(3, user.getPasswordHash());
        statement.setString(4, user.getRole().name());
        statement.executeUpdate();
        connection.commit();
        statement.close();
    }

    @Override
    public boolean isLoginExist(@NotNull final String login) throws SQLException {
        @NotNull final String query = "SELECT EXISTS (SELECT login from app_user WHERE login = ?)";
        @NotNull final PreparedStatement statement = connection.prepareStatement(query);
        statement.setString(1, login);
        @NotNull final ResultSet resultSet = statement.executeQuery();
        if (!resultSet.next()) return false;
        @NotNull boolean result = resultSet.getInt(1) >= 1;
        statement.close();
        return result;
    }

    @Override
    public @Nullable User findUserByLogin(@NotNull final String login) throws SQLException {
        @NotNull final String query = "SELECT * FROM app_user WHERE login = ?";
        @NotNull final PreparedStatement statement = connection.prepareStatement(query);
        statement.setString(1, login);
        @NotNull final ResultSet resultSet = statement.executeQuery();
        if (!resultSet.next()) return null;
        @Nullable final User user = fetch(resultSet);
        resultSet.close();
        statement.close();
        return user;
    }

    @Nullable
    private User fetch(@Nullable final ResultSet row) throws SQLException {
        if (row == null) return null;
        @NotNull final User user = new User();
        user.setId(row.getString(FiledConstant.ID));
        user.setPasswordHash(row.getString(FiledConstant.PASSWORD_HASH));
        user.setRole(Role.valueOf(row.getString(FiledConstant.ROLE)));
        user.setLogin(row.getString(FiledConstant.LOGIN));
        return user;
    }

}