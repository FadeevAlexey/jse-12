package ru.fadeev.tm.repository;

import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.fadeev.tm.api.repository.ITaskRepository;
import ru.fadeev.tm.entity.Task;
import ru.fadeev.tm.enumerated.Status;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.*;

import static ru.fadeev.tm.constant.FiledConstant.*;
import static ru.fadeev.tm.constant.FiledConstant.CREATE_DATE;
import static ru.fadeev.tm.util.DbUtil.getSqlDate;

@Setter
@NoArgsConstructor
public final class TaskRepository extends AbstractRepository<Task> implements ITaskRepository {

    @NotNull
    private Connection connection;

    public TaskRepository(@NotNull final Connection connection) {
        this.connection = connection;
    }

    @Override
    public @NotNull List<Task> findAll() throws SQLException {
        @NotNull final String query = "SELECT * FROM app_task";
        @NotNull final PreparedStatement statement = connection.prepareStatement(query);
        @NotNull final ResultSet resultSet = statement.executeQuery();
        if (resultSet == null) return Collections.emptyList();
        @NotNull final List<Task> tasks = new ArrayList<>();
        while (resultSet.next()) tasks.add(fetch(resultSet));
        statement.close();
        resultSet.close();
        return tasks;
    }

    @Nullable
    @Override
    public Task findOne(@NotNull final String id) throws SQLException {
        @NotNull final String query = "SELECT * FROM app_task WHERE id = ?";
        @NotNull final PreparedStatement statement = connection.prepareStatement(query);
        statement.setString(1, id);
        @NotNull final ResultSet resultSet = statement.executeQuery();
        if (!resultSet.next()) return null;
        @Nullable final Task task = fetch(resultSet);
        resultSet.close();
        statement.close();
        return task;
    }

    @Nullable
    @Override
    public Task findOne(@NotNull final String userId, @NotNull final String id) throws SQLException {
        @NotNull final String query = "SELECT * FROM app_task WHERE userId = ? and id = ?";
        @NotNull final PreparedStatement statement = connection.prepareStatement(query);
        statement.setString(1, userId);
        statement.setString(2, id);
        @NotNull final ResultSet resultSet = statement.executeQuery();
        if (!resultSet.next()) return null;
        @Nullable final Task task = fetch(resultSet);
        resultSet.close();
        statement.close();
        return task;
    }

    @Override
    public void remove(@NotNull String id) throws SQLException {
        @NotNull final String query = "DELETE FROM app_task WHERE id = ?";
        @NotNull final PreparedStatement statement = connection.prepareStatement(query);
        statement.setString(1, id);
        statement.executeUpdate();
        statement.close();
        connection.commit();
    }

    @Override
    public void remove(@NotNull final String userId, @NotNull final String taskId) throws SQLException {
        @NotNull final String query = "DELETE FROM app_task WHERE userId = ? AND id = ?";
        @NotNull final PreparedStatement statement = connection.prepareStatement(query);
        statement.setString(1, userId);
        statement.setString(2, taskId);
        statement.executeUpdate();
        statement.close();
        connection.commit();
    }

    @Override
    public void persist(Task task) throws Exception {
        @NotNull final String query = "INSERT INTO app_task VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?)";
        @NotNull final PreparedStatement statement = connection.prepareStatement(query);
        statement.setString(1, task.getId());
        statement.setString(2, task.getName());
        statement.setString(3, task.getDescription());
        statement.setString(4, task.getProjectId());
        statement.setDate(5, getSqlDate(task.getStartDate()));
        statement.setDate(6, getSqlDate(task.getFinishDate()));
        statement.setString(7, task.getUserId());
        statement.setString(8, task.getStatus().name());
        statement.setDate(9, getSqlDate(task.getCreationDate()));
        statement.executeUpdate();
        connection.commit();
        statement.close();
    }

    @Override
    public void merge(@NotNull Task task) throws Exception {
        @NotNull final String query =
                "UPDATE app_task " +
                        "SET name = ?, description = ?, projectId = ?, startDate = ?, finishDate = ?, userId = ?, status = ?, creationTime = ?" +
                        "WHERE id = ?";
        @NotNull final PreparedStatement statement = connection.prepareStatement(query);
        statement.setString(1, task.getName());
        statement.setString(2, task.getDescription());
        statement.setString(3, task.getProjectId());
        statement.setDate(4, getSqlDate(task.getStartDate()));
        statement.setDate(5, getSqlDate(task.getFinishDate()));
        statement.setString(6, task.getUserId());
        statement.setString(7, task.getStatus().name());
        statement.setDate(8, getSqlDate(task.getCreationDate()));
        statement.setString(9, task.getId());
        statement.executeUpdate();
        statement.close();
        connection.commit();
    }

    @Override
    public void removeAll() throws SQLException {
        @NotNull final String query = "DELETE FROM app_task";
        @NotNull final PreparedStatement statement = connection.prepareStatement(query);
        statement.executeUpdate();
        statement.close();
        connection.commit();
    }

    @Override
    public void removeAll(@NotNull final String userId) throws SQLException {
        @NotNull final String query = "DELETE FROM app_task WHERE userId = ?";
        @NotNull final PreparedStatement statement = connection.prepareStatement(query);
        statement.setString(1, userId);
        statement.executeUpdate();
        statement.close();
        connection.commit();
    }

    @NotNull
    @Override
    public Collection<Task> findAll(@NotNull final String userId) throws SQLException {
        @NotNull final String query = "SELECT * FROM app_task WHERE userId = ?";
        @NotNull final PreparedStatement statement = connection.prepareStatement(query);
        statement.setString(1, userId);
        @NotNull final ResultSet resultSet = statement.executeQuery();
        if (resultSet == null) return Collections.emptyList();
        @NotNull final List<Task> taskList = new ArrayList<>();
        while (resultSet.next()) taskList.add(fetch(resultSet));
        statement.close();
        resultSet.close();
        return taskList;
    }

    @NotNull
    @Override
    public Collection<Task> findAllByProjectId(@NotNull final String projectId, @NotNull final String userId) throws SQLException {
        @NotNull final String query = "SELECT * FROM app_task WHERE projectId = ?";
        @NotNull final PreparedStatement statement = connection.prepareStatement(query);
        statement.setString(1, projectId);
        @NotNull final ResultSet resultSet = statement.executeQuery();
        if (resultSet == null) return Collections.emptyList();
        @NotNull final List<Task> tasks = new ArrayList<>();
        while (resultSet.next()) tasks.add(fetch(resultSet));
        statement.close();
        resultSet.close();
        return tasks;
    }

    @Nullable
    @Override
    public String findIdByName(@NotNull final String userId, @NotNull final String name) throws SQLException {
        @NotNull final String query = "SELECT id FROM app_task WHERE userId = ? and name = ?";
        @NotNull final PreparedStatement statement = connection.prepareStatement(query);
        statement.setString(1, userId);
        statement.setString(2, name);
        @NotNull final ResultSet resultSet = statement.executeQuery();
        if (resultSet == null) return null;
        resultSet.next();
        @NotNull final String id = resultSet.getString(ID);
        statement.close();
        resultSet.close();
        return id;
    }

    @Override
    public void removeAllByProjectId(@NotNull final String userId, @NotNull final String projectId) throws SQLException {
        @NotNull final String query = "DELETE FROM app_task WHERE userId = ? AND projectId = ?";
        @NotNull final PreparedStatement statement = connection.prepareStatement(query);
        statement.setString(1, userId);
        statement.setString(2, projectId);
        statement.executeUpdate();
        statement.close();
        connection.commit();
    }

    @Override
    public void removeAllProjectTask(@NotNull final String userId) throws SQLException {
        @NotNull final String query = "DELETE FROM app_task WHERE userId = ? AND projectId IS NOT null";
        @NotNull final PreparedStatement statement = connection.prepareStatement(query);
        statement.setString(1, userId);
        statement.executeUpdate();
        statement.close();
        connection.commit();
    }

    @NotNull
    public Collection<Task> searchByName(@NotNull final String userId, @NotNull final String name) throws SQLException {
        @NotNull final String query = "SELECT * FROM app_task WHERE userId = ? AND name LIKE CONCAT('%',?,'%')";
        @NotNull final PreparedStatement statement = connection.prepareStatement(query);
        statement.setString(1, userId);
        statement.setString(2, name);
        @NotNull final ResultSet resultSet = statement.executeQuery();
        if (resultSet == null) return Collections.emptyList();
        @NotNull final List<Task> tasks = new ArrayList<>();
        while (resultSet.next()) tasks.add(fetch(resultSet));
        statement.close();
        resultSet.close();
        return tasks;
    }

    @NotNull
    @Override
    public Collection<Task> searchByDescription(@NotNull final String userId, @NotNull final String description) throws SQLException {
        @NotNull final String query = "SELECT * FROM app_task WHERE userId = ? AND description LIKE CONCAT('%',?,'%')";
        @NotNull final PreparedStatement statement = connection.prepareStatement(query);
        statement.setString(1, userId);
        statement.setString(2, description);
        @NotNull final ResultSet resultSet = statement.executeQuery();
        if (resultSet == null) return Collections.emptyList();
        @NotNull final List<Task> tasks = new ArrayList<>();
        while (resultSet.next()) tasks.add(fetch(resultSet));
        statement.close();
        resultSet.close();
        return tasks;
    }

    @NotNull
    @Override
    public Collection<Task> sortAllByStartDate(@NotNull final String userId, @NotNull final String sortType) throws SQLException {
        @NotNull String query = "SELECT * FROM app_task WHERE userId = ? ORDER BY startDate";
        if ("desc".equals(sortType.toLowerCase()))
            query = "SELECT * FROM app_task WHERE userId = ? ORDER BY startDate DESC";
        @NotNull final PreparedStatement statement = connection.prepareStatement(query);
        statement.setString(1, userId);
        @NotNull final ResultSet resultSet = statement.executeQuery();
        if (resultSet == null) return Collections.emptyList();
        @NotNull final List<Task> tasks = new ArrayList<>();
        while (resultSet.next()) tasks.add(fetch(resultSet));
        statement.close();
        resultSet.close();
        return tasks;
    }

    @NotNull
    @Override
    public Collection<Task> sortAllByFinishDate(@NotNull final String userId, @NotNull final String sortType) throws SQLException {
        @NotNull String query = "SELECT * FROM app_task WHERE userId = ? ORDER BY finishDate";
        if ("desc".equals(sortType.toLowerCase()))
            query = "SELECT * FROM app_task WHERE userId = ? ORDER BY finishDate DESC";
        @NotNull final PreparedStatement statement = connection.prepareStatement(query);
        statement.setString(1, userId);
        @NotNull final ResultSet resultSet = statement.executeQuery();
        if (resultSet == null) return Collections.emptyList();
        @NotNull final List<Task> tasks = new ArrayList<>();
        while (resultSet.next()) tasks.add(fetch(resultSet));
        statement.close();
        resultSet.close();
        return tasks;
    }

    @NotNull
    @Override
    public Collection<Task> sortAllByStatus(@NotNull final String userId, @NotNull final String sortType) throws SQLException {
        @NotNull String query = "SELECT * FROM app_task WHERE userId = ? ORDER BY status";
        if ("desc".equals(sortType.toLowerCase()))
            query = "SELECT * FROM app_task WHERE userId = ? ORDER BY status DESC";
        @NotNull final PreparedStatement statement = connection.prepareStatement(query);
        statement.setString(1, userId);
        @NotNull final ResultSet resultSet = statement.executeQuery();
        if (resultSet == null) return Collections.emptyList();
        List<Task> tasks = new ArrayList<>();
        while (resultSet.next()) tasks.add(fetch(resultSet));
        statement.close();
        resultSet.close();
        return tasks;
    }

    @NotNull
    @Override
    public Collection<Task> sortAllByCreationDate(@NotNull final String userId) throws SQLException {
        @NotNull final String query = "SELECT * FROM app_task WHERE userId = ? ORDER BY status";
        @NotNull final PreparedStatement statement = connection.prepareStatement(query);
        statement.setString(1, userId);
        @NotNull final ResultSet resultSet = statement.executeQuery();
        if (resultSet == null) return Collections.emptyList();
        List<Task> tasks = new ArrayList<>();
        while (resultSet.next()) tasks.add(fetch(resultSet));
        statement.close();
        resultSet.close();
        return tasks;
    }

    @Nullable
    private Task fetch(@Nullable final ResultSet row) throws SQLException {
        if (row == null) return null;
        @NotNull final Task task = new Task();
        task.setId(row.getString(ID));
        task.setName(row.getString(NAME));
        task.setDescription(row.getString(DESCRIPTION));
        task.setProjectId(row.getString(PROJECT_ID));
        task.setStartDate(row.getDate(START_DATE));
        task.setFinishDate(row.getDate(FINISH_DATE));
        task.setUserId(row.getString(USER_ID));
        task.setStatus(Status.valueOf(row.getString(STATUS)));
        task.setCreationDate(row.getDate(CREATE_DATE));
        return task;
    }

}