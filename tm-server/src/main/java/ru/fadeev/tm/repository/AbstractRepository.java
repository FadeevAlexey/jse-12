package ru.fadeev.tm.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.fadeev.tm.api.repository.IRepository;
import ru.fadeev.tm.entity.AbstractEntity;

import java.sql.SQLException;
import java.util.List;

public abstract class AbstractRepository<E extends AbstractEntity> implements IRepository<E> {

    @NotNull
    @Override
    public abstract List<E> findAll() throws SQLException;

    @Nullable
    @Override
    public abstract E findOne(@NotNull String id) throws SQLException;

    @Override
    public abstract void remove(@NotNull String id) throws SQLException;

    @Override
    public abstract void persist(E e) throws Exception;

    @Override
    public abstract void merge(@NotNull E e) throws Exception;

    @Override
    public abstract void removeAll() throws SQLException;

}