package ru.fadeev.tm.util;

import org.jetbrains.annotations.Nullable;

import java.sql.Connection;
import java.sql.Date;
import java.sql.DriverManager;

public final class DbUtil {

    public static Connection createConnection(String url) throws Exception {
        Class.forName("com.mysql.jdbc.Driver");
        return DriverManager.getConnection(url);
    }

    @Nullable
    public static Date getSqlDate(@Nullable final java.util.Date date){
        if (date == null) return null;
        return new Date(date.getTime());
    }

}