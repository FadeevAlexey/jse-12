package ru.fadeev.tm.constant;

public class FiledConstant {

    public static final String ID = "id";

    public static final String LOGIN = "login";

    public static final String PASSWORD_HASH = "passwordHash";

    public static final String ROLE = "role";

    public static final String USER_ID = "userId";

    public static final String SIGNATURE = "signature";

    public static final String CREATION_TIME = "creationTime";

    public static final String NAME = "name";

    public static final String DESCRIPTION = "description";

    public static final String START_DATE = "startDate";

    public static final String FINISH_DATE = "finishDate";

    public static final String STATUS = "status";

    public static final String CREATE_DATE = "creationTime";

    public static final String PROJECT_ID = "projectId";

}
