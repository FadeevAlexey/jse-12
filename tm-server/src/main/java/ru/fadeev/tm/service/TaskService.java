package ru.fadeev.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.fadeev.tm.api.service.ITaskService;
import ru.fadeev.tm.api.service.ServiceLocator;
import ru.fadeev.tm.constant.Sort;
import ru.fadeev.tm.entity.Task;
import ru.fadeev.tm.api.repository.ITaskRepository;
import ru.fadeev.tm.exception.IllegalSortTypeException;
import ru.fadeev.tm.repository.TaskRepository;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.*;

public final class TaskService extends AbstractService<Task> implements ITaskService {

    public TaskService(@NotNull final ServiceLocator serviceLocator) {
        super(serviceLocator);
    }

    @Override
    public @NotNull List<Task> findAll() throws Exception {
        @NotNull final Connection connection = getConnection();
        @NotNull final ITaskRepository repository = new TaskRepository(connection);
        @NotNull final List<Task> tasks = repository.findAll();
        connection.close();
        return tasks;
    }

    @Nullable
    @Override
    public Task findOne(@Nullable final String id) throws Exception {
        if (id == null || id.isEmpty()) return null;
        @NotNull final Connection connection = getConnection();
        @NotNull final ITaskRepository repository = new TaskRepository(connection);
        @Nullable final Task task = repository.findOne(id);
        connection.close();
        return task;
    }

    @Nullable
    @Override
    public Task findOne(@Nullable final String userId, final String id) throws Exception {
        if (userId == null || userId.isEmpty()) return null;
        if (id == null || id.isEmpty()) return null;
        @NotNull final Connection connection = getConnection();
        @NotNull final ITaskRepository repository = new TaskRepository(connection);
        @Nullable final Task task = repository.findOne(userId, id);
        connection.close();
        return task;
    }

    @Override
    public void remove(@Nullable final String id) throws Exception {
        if (id == null || id.isEmpty()) return;
        @NotNull final Connection connection = getConnection();
        connection.setAutoCommit(false);
        @NotNull final ITaskRepository repository = new TaskRepository(connection);
        try {
            repository.remove(id);
        } catch (SQLException e) {
            connection.rollback();
            throw new Exception("Can't remove task");
        } finally {
            connection.close();
        }
    }

    @Override
    public void remove(@Nullable final String userId, @Nullable final String projectId) throws Exception {
        if (userId == null || userId.isEmpty()) return;
        if (projectId == null || projectId.isEmpty()) return;
        @NotNull final Connection connection = getConnection();
        connection.setAutoCommit(false);
        @NotNull final ITaskRepository repository = new TaskRepository(connection);
        try {
            repository.remove(userId, projectId);
        } catch (SQLException e) {
            connection.rollback();
            throw new Exception("Can't remove task");
        } finally {
            connection.close();
        }
    }

    @Override
    public void persist(@Nullable final Task task) throws Exception {
        if (task == null) return;
        @NotNull final Connection connection = getConnection();
        connection.setAutoCommit(false);
        @NotNull final ITaskRepository repository = new TaskRepository(connection);
        try {
            repository.persist(task);
        } catch (SQLException e) {
            connection.rollback();
            throw new Exception("Can't add task");
        } finally {
            connection.close();
        }
    }

    @Override
    public void merge(@Nullable final Task task) throws Exception {
        if (task == null) return;
        @NotNull final Connection connection = getConnection();
        connection.setAutoCommit(false);
        try {
            @NotNull final ITaskRepository repository = new TaskRepository(connection);
            repository.merge(task);
        } catch (SQLException e) {
            connection.rollback();
            throw new Exception("Can't update task");
        } finally {
            connection.close();
        }
    }

    @Override
    public void removeAll() throws Exception {
        @NotNull final Connection connection = getConnection();
        @NotNull final ITaskRepository repository = new TaskRepository(connection);
        connection.setAutoCommit(false);
        try {
            repository.removeAll();
        } catch (SQLException e) {
            connection.rollback();
            throw new Exception("Can't remove tasks");
        } finally {
            connection.close();
        }
    }

    @Override
    public void removeAll(@Nullable final String userId) throws Exception {
        if (userId == null || userId.isEmpty()) return;
        @NotNull final Connection connection = getConnection();
        @NotNull final ITaskRepository repository = new TaskRepository(connection);
        connection.setAutoCommit(false);
        try {
            repository.removeAll(userId);
        } catch (SQLException e) {
            connection.rollback();
            throw new Exception("Can't remove tasks");
        } finally {
            connection.close();
        }
    }

    @NotNull
    @Override
    public Collection<Task> findAll(@Nullable final String userId) throws Exception {
        if (userId == null || userId.isEmpty()) return new ArrayList<>();
        @NotNull final Connection connection = getConnection();
        @NotNull final ITaskRepository repository = new TaskRepository(connection);
        @NotNull final Collection<Task> projects = repository.findAll(userId);
        connection.close();
        return projects;
    }

    @Override
    @NotNull
    public Collection<Task> findAllByProjectId(@Nullable final String projectId, @Nullable final String userId) throws Exception {
        if (projectId == null || projectId.isEmpty()) return Collections.emptyList();
        if (userId == null || userId.isEmpty()) return Collections.emptyList();
        @NotNull final Connection connection = getConnection();
        @NotNull final ITaskRepository repository = new TaskRepository(connection);
        @NotNull final Collection<Task> projects = repository.findAllByProjectId(projectId, userId);
        connection.close();
        return projects;
    }

    @Nullable
    @Override
    public String findIdByName(@Nullable final String userId, @Nullable final String name) throws Exception {
        if (name == null || name.isEmpty()) return null;
        if (userId == null || userId.isEmpty()) return null;
        @NotNull final Connection connection = getConnection();
        @NotNull final ITaskRepository repository = new TaskRepository(connection);
        @Nullable final String id = repository.findIdByName(userId, name);
        connection.close();
        return id;
    }

    @Override
    public void removeAllByProjectId(@Nullable final String userId, @Nullable final String projectId) throws Exception {
        if (projectId == null || projectId.isEmpty()) return;
        if (userId == null || userId.isEmpty()) return;
        @NotNull final Connection connection = getConnection();
        @NotNull final ITaskRepository repository = new TaskRepository(connection);
        connection.setAutoCommit(false);
        try {
            repository.removeAllByProjectId(userId, projectId);
        } catch (SQLException e) {
            connection.rollback();
            throw new Exception("Can't remove task");
        } finally {
            connection.close();
        }
    }

    @Override
    public void removeAllProjectTask(@Nullable final String userId) throws Exception {
        if (userId == null || userId.isEmpty()) return;
        @NotNull final Connection connection = getConnection();
        @NotNull final ITaskRepository repository = new TaskRepository(connection);
        connection.setAutoCommit(false);
        try {
            repository.removeAllProjectTask(userId);
        } catch (SQLException e) {
            connection.rollback();
            throw new Exception("Can't remove task");
        } finally {
            connection.close();
        }
    }

    @NotNull
    @Override
    public Collection<Task> searchByName(@Nullable final String userId, @Nullable final String string) throws Exception {
        if (userId == null || userId.isEmpty()) return Collections.emptyList();
        if (string == null || string.isEmpty()) return Collections.emptyList();
        @NotNull final Connection connection = getConnection();
        @NotNull final ITaskRepository repository = new TaskRepository(connection);
        @NotNull final Collection<Task> projects = repository.searchByName(userId, string);
        connection.close();
        return projects;
    }

    @NotNull
    @Override
    public Collection<Task> searchByDescription(@Nullable final String userId, @Nullable final String string) throws Exception {
        if (userId == null || userId.isEmpty()) return Collections.emptyList();
        if (string == null || string.isEmpty()) return Collections.emptyList();
        @NotNull final Connection connection = getConnection();
        @NotNull final ITaskRepository repository = new TaskRepository(connection);
        @NotNull final Collection<Task> tasks = repository.searchByDescription(userId, string);
        connection.close();
        return tasks;
    }

    @NotNull
    @Override
    public Collection<Task> sortAllTask(@Nullable final String userId, @Nullable String sortRequest) throws Exception {
        @NotNull String sortType = Sort.SORT_ASC;
        if (userId == null || userId.isEmpty()) return Collections.emptyList();
        if (sortRequest == null || sortRequest.isEmpty()) return sortByCreationDate(userId);
        if (sortRequest.toUpperCase().contains(Sort.SUFFIX)) {
            sortRequest = sortRequest.substring(0, sortRequest.length() - Sort.SUFFIX.length());
            sortType = Sort.SORT_DESC;
        }
        switch (sortRequest.toLowerCase()) {
            case "start date":
                return sortByStartDate(userId, sortType);
            case "finish date":
                return sortByFinishDate(userId, sortType);
            case "status":
                return sortByStatus(userId, sortType);
            default:
                throw new IllegalSortTypeException("cannot sort");
        }
    }

    private Collection<Task> sortByStartDate(@Nullable final String userId, @Nullable String sortType) throws Exception {
        if (userId == null || userId.isEmpty()) return Collections.emptyList();
        if (sortType == null || sortType.isEmpty()) sortType = "ASC";
        @NotNull final Connection connection = getConnection();
        @NotNull final ITaskRepository repository = new TaskRepository(connection);
        @NotNull final Collection<Task> tasks = repository.sortAllByStartDate(userId, sortType);
        connection.close();
        return tasks;
    }

    private Collection<Task> sortByFinishDate(@Nullable final String userId, @Nullable String sortType) throws Exception {
        if (userId == null || userId.isEmpty()) return Collections.emptyList();
        if (sortType == null || sortType.isEmpty()) sortType = "ASC";
        @NotNull final Connection connection = getConnection();
        @NotNull final ITaskRepository repository = new TaskRepository(connection);
        @NotNull final Collection<Task> tasks = repository.sortAllByFinishDate(userId, sortType);
        connection.close();
        return tasks;
    }

    private Collection<Task> sortByStatus(@Nullable final String userId, @Nullable String sortType) throws Exception {
        if (userId == null || userId.isEmpty()) return Collections.emptyList();
        if (sortType == null || sortType.isEmpty()) sortType = "ASC";
        @NotNull final Connection connection = getConnection();
        @NotNull final ITaskRepository repository = new TaskRepository(connection);
        @NotNull final Collection<Task> tasks = repository.sortAllByStatus(userId, sortType);
        connection.close();
        return tasks;
    }

    private Collection<Task> sortByCreationDate(@Nullable final String userId) throws Exception {
        if (userId == null || userId.isEmpty()) return Collections.emptyList();
        @NotNull final Connection connection = getConnection();
        @NotNull final ITaskRepository repository = new TaskRepository(connection);
        @NotNull final Collection<Task> tasks = repository.sortAllByCreationDate(userId);
        connection.close();
        return tasks;
    }

}