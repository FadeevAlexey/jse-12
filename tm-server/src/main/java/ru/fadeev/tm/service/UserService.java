package ru.fadeev.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.fadeev.tm.api.service.IUserService;
import ru.fadeev.tm.api.service.ServiceLocator;
import ru.fadeev.tm.entity.User;
import ru.fadeev.tm.enumerated.Role;
import ru.fadeev.tm.api.repository.IUserRepository;
import ru.fadeev.tm.repository.UserRepository;
import ru.fadeev.tm.util.PasswordHashUtil;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.List;

public final class UserService extends AbstractService<User> implements IUserService {

    public UserService(@NotNull final ServiceLocator serviceLocator) {
        super(serviceLocator);
    }

    @Override
    public @NotNull List<User> findAll() throws Exception {
        @NotNull final Connection connection = getConnection();
        @NotNull final UserRepository repository = new UserRepository(connection);
        @NotNull final List<User> users = repository.findAll();
        connection.close();
        return users;
    }

    @Nullable
    @Override
    public User findOne(@Nullable final String id) throws Exception {
        if (id == null || id.isEmpty()) return null;
        @NotNull final Connection connection = getConnection();
        @NotNull final UserRepository repository = new UserRepository(connection);
        @Nullable final User user = repository.findOne(id);
        connection.close();
        return user;
    }

    @Override
    public void remove(@Nullable final String id) throws Exception {
        if (id == null || id.isEmpty()) return;
        @NotNull final Connection connection = getConnection();
        connection.setAutoCommit(false);
        @NotNull final UserRepository repository = new UserRepository(connection);
        try {
            repository.remove(id);
        } catch (SQLException e) {
            connection.rollback();
            throw new Exception("Can't remove user");
        } finally {
            connection.close();
        }
    }

    @Override
    public void removeAll() throws Exception {
        @NotNull final Connection connection = getConnection();
        connection.setAutoCommit(false);
        @NotNull final UserRepository repository = new UserRepository(connection);
        try {
            repository.removeAll();
        } catch (SQLException e) {
            connection.rollback();
            throw new Exception("Can't remove user");
        } finally {
            connection.close();
        }
    }

    @Override
    public void persist(@Nullable final User user) throws Exception {
        if (user == null) return;
        user.setPasswordHash(PasswordHashUtil.md5(user.getPasswordHash()));
        @NotNull final Connection connection = getConnection();
        @NotNull final IUserRepository repository = new UserRepository(connection);
        connection.setAutoCommit(false);
        try {
            repository.persist(user);
        } catch (SQLException e) {
            connection.rollback();
            throw new Exception("Can't create user");
        } finally {
            connection.close();
        }
    }

    @Override
    public void merge(@Nullable final User user) throws Exception {
        if (user == null) return;
        user.setPasswordHash(PasswordHashUtil.md5(user.getPasswordHash()));
        @NotNull final Connection connection = getConnection();
        @NotNull final IUserRepository repository = new UserRepository(connection);
        connection.setAutoCommit(false);
        try {
            repository.merge(user);
        } catch (SQLException e) {
            connection.rollback();
            throw new Exception("Can't update user");
        } finally {
            connection.close();
        }
    }

    @Override
    public boolean isLoginExist(@Nullable final String login) throws Exception {
        if (login == null || login.isEmpty()) return false;
        @NotNull final Connection connection = getConnection();
        @NotNull final IUserRepository repository = new UserRepository(connection);
        final boolean isLoginExists = repository.isLoginExist(login);
        connection.close();
        return isLoginExists;
    }

    @Nullable
    @Override
    public User findUserByLogin(@Nullable final String login) throws Exception {
        if (login == null || login.isEmpty()) return null;
        @NotNull final Connection connection = getConnection();
        @NotNull final IUserRepository repository = new UserRepository(connection);
        @Nullable final User user = repository.findUserByLogin(login);
        connection.close();
        return user;
    }

    @Override
    public void setAdminRole(@Nullable final User user) throws Exception {
        if (user == null) return;
        user.setRole(Role.ADMINISTRATOR);
        merge(user);
    }

}