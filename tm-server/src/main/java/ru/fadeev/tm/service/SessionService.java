package ru.fadeev.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.fadeev.tm.api.repository.ISessionRepository;
import ru.fadeev.tm.api.service.ISessionService;
import ru.fadeev.tm.api.service.ServiceLocator;
import ru.fadeev.tm.entity.Session;
import ru.fadeev.tm.entity.User;
import ru.fadeev.tm.enumerated.Role;
import ru.fadeev.tm.exception.AccessDeniedException;
import ru.fadeev.tm.exception.InvalidSessionException;
import ru.fadeev.tm.repository.SessionRepository;
import ru.fadeev.tm.util.PasswordHashUtil;
import ru.fadeev.tm.util.SignatureUtil;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.Date;
import java.util.List;

public class SessionService extends AbstractService<Session> implements ISessionService {

    public SessionService(@NotNull final ServiceLocator serviceLocator) {
        super(serviceLocator);
    }

    @NotNull
    @Override
    public List<Session> findAll() throws Exception {
        @NotNull final Connection connection = getConnection();
        @NotNull final ISessionRepository repository = new SessionRepository(connection);
        @NotNull final List<Session> users = repository.findAll();
        connection.close();
        return users;
    }

    @Nullable
    @Override
    public Session findOne(@Nullable final String id) throws Exception {
        if (id == null || id.isEmpty()) return null;
        @NotNull final Connection connection = getConnection();
        @NotNull final ISessionRepository repository = new SessionRepository(connection);
        @Nullable final Session session = repository.findOne(id);
        connection.close();
        return session;
    }

    @Override
    public void remove(@Nullable final String id) throws Exception {
        if (id == null || id.isEmpty()) return;
        @NotNull final Connection connection = getConnection();
        connection.setAutoCommit(false);
        @NotNull ISessionRepository repository = new SessionRepository(connection);
        try {
            repository.remove(id);
        } catch (SQLException e) {
            connection.rollback();
            throw new Exception("Can't remove session");
        } finally {
            connection.close();
        }
    }

    @Override
    public void persist(@Nullable final Session session) throws Exception {
        if (session == null) return;
        @NotNull final Connection connection = getConnection();
        @NotNull final ISessionRepository repository = new SessionRepository(connection);
        connection.setAutoCommit(false);
        try {
            repository.persist(session);
        } catch (SQLException e) {
            connection.rollback();
            throw new Exception("Can't create session");
        } finally {
            connection.close();
        }
    }

    @Override
    public void merge(@Nullable final Session session) throws Exception {
        if (session == null) return;
        @NotNull final Connection connection = getConnection();
        @NotNull final ISessionRepository repository = new SessionRepository(connection);
        connection.setAutoCommit(false);
        try {
            repository.merge(session);
        } catch (SQLException e) {
            connection.rollback();
            throw new Exception("Can't update session");
        } finally {
            connection.close();
        }
    }

    @Override
    public void removeAll() throws Exception {
        @NotNull final Connection connection = getConnection();
        connection.setAutoCommit(false);
        @NotNull final ISessionRepository repository = new SessionRepository(connection);
        try {
            repository.removeAll();
        } catch (SQLException e) {
            connection.rollback();
            throw new Exception("Can't remove sessions");
        } finally {
            connection.close();
        }
    }

    @Nullable
    @Override
    public Session openSession(@Nullable final String login, @Nullable String password) throws Exception {
        if (login == null || login.isEmpty()) return null;
        if (password == null || password.isEmpty()) return null;
        @Nullable final User user = serviceLocator.getUserService().findUserByLogin(login);
        if (user == null) return null;
        password = PasswordHashUtil.md5(password);
        if (password == null) return null;
        if (!password.equals(user.getPasswordHash())) return null;
        Session session = new Session();
        session.setUserId(user.getId());
        session.setRole(user.getRole());
        session.setSignature(SignatureUtil.sign(session,
                serviceLocator.getPropertyService().getSessionSalt(),
                serviceLocator.getPropertyService().getSessionCycle()));
        persist(session);
        return session;
    }

    @Override
    public void closeSession(@Nullable final Session session) throws Exception {
        if (session == null) return;
        if (session.getSignature() == null) return;
        @NotNull final Connection connection = getConnection();
        connection.setAutoCommit(false);
        @NotNull ISessionRepository repository = new SessionRepository(connection);
        try {
            repository.removeSessionBySignature(session.getSignature());
        } catch (SQLException e) {
            connection.rollback();
            throw new Exception("Can't close session");
        } finally {
            connection.close();
        }
    }

    @Override
    public boolean contains(@Nullable final String sessionId) throws Exception {
        if (sessionId == null) return false;
        @NotNull final Connection connection = getConnection();
        @NotNull final ISessionRepository repository = new SessionRepository(connection);
        @NotNull final boolean contains = repository.contains(sessionId);
        connection.close();
        return contains;
    }

    public void checkSession(@Nullable final Session currentSession) throws Exception {
        final long currentTime = new Date().getTime();
        if (currentSession == null) throw new InvalidSessionException("Invalid session");
        if (!contains(currentSession.getId())) throw new InvalidSessionException("Invalid session");
        if (currentSession.getUserId() == null) throw new InvalidSessionException("Invalid session");
        if (currentSession.getSignature() == null) throw new InvalidSessionException("Invalid session");
        if (currentSession.getRole() == null) throw new InvalidSessionException("Invalid session");
        @NotNull final Session session = new Session();
        session.setSignature(currentSession.getSignature());
        session.setRole(currentSession.getRole());
        session.setId(currentSession.getId());
        session.setCreationTime(currentSession.getCreationTime());
        session.setUserId(currentSession.getUserId());
        @Nullable final String sessionSignature =
                SignatureUtil.sign(session,
                        serviceLocator.getPropertyService().getSessionSalt(),
                        serviceLocator.getPropertyService().getSessionCycle());
        @Nullable final String currentSessionSignature =
                SignatureUtil.sign(currentSession,
                        serviceLocator.getPropertyService().getSessionSalt(),
                        serviceLocator.getPropertyService().getSessionCycle());
        if (sessionSignature == null || currentSessionSignature == null)
            throw new InvalidSessionException("Invalid session");
        if (!sessionSignature.equals(currentSessionSignature)) throw new InvalidSessionException("Invalid session");
        if (currentSession.getCreationTime() - currentTime > serviceLocator.getPropertyService().getSessionLifetime())
            throw new InvalidSessionException("Invalid session");
    }

    public void checkSession(@Nullable final Session currentSession, @NotNull final Role role) throws Exception {
        if (currentSession == null) throw new InvalidSessionException("Invalid session");
        checkSession(currentSession);
        if (currentSession.getRole() != role) throw new AccessDeniedException("Access denied");
    }

}