package ru.fadeev.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.fadeev.tm.api.repository.IProjectRepository;
import ru.fadeev.tm.api.service.IProjectService;
import ru.fadeev.tm.api.service.ServiceLocator;
import ru.fadeev.tm.constant.Sort;
import ru.fadeev.tm.entity.Project;
import ru.fadeev.tm.exception.IllegalSortTypeException;
import ru.fadeev.tm.repository.ProjectRepository;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.*;

public final class ProjectService extends AbstractService<Project> implements IProjectService {

    public ProjectService(@NotNull final ServiceLocator serviceLocator) {
        super(serviceLocator);
    }

    @Override
    public @NotNull List<Project> findAll() throws Exception {
        @NotNull final Connection connection = getConnection();
        @NotNull final IProjectRepository repository = new ProjectRepository(connection);
        @NotNull final List<Project> projects = repository.findAll();
        connection.close();
        return projects;
    }

    @Nullable
    @Override
    public Project findOne(@Nullable final String id) throws Exception {
        if (id == null || id.isEmpty()) return null;
        @NotNull final Connection connection = getConnection();
        @NotNull final IProjectRepository repository = new ProjectRepository(connection);
        @Nullable final Project project = repository.findOne(id);
        connection.close();
        return project;
    }

    @Nullable
    @Override
    public Project findOne(@Nullable final String userId, final String id) throws Exception {
        if (userId == null || userId.isEmpty()) return null;
        if (id == null || id.isEmpty()) return null;
        @NotNull final Connection connection = getConnection();
        @NotNull final IProjectRepository repository = new ProjectRepository(connection);
        @Nullable final Project project = repository.findOne(userId,id);
        connection.close();
        return project;
    }

    @Override
    public void remove(@Nullable final String id) throws Exception {
        if (id == null || id.isEmpty()) return;
        @NotNull final Connection connection = getConnection();
        connection.setAutoCommit(false);
        @NotNull IProjectRepository repository = new ProjectRepository(connection);
        try {
            repository.remove(id);
        } catch (SQLException e) {
            connection.rollback();
            throw new Exception("Can't remove project");
        } finally {
            connection.close();
        }
    }

    @Override
    public void remove(@Nullable final String userId ,@Nullable final String projectId) throws Exception {
        if (userId == null || userId.isEmpty()) return;
        if (projectId == null || projectId.isEmpty()) return;
        @NotNull final Connection connection = getConnection();
        connection.setAutoCommit(false);
        @NotNull IProjectRepository repository = new ProjectRepository(connection);
        try {
            repository.remove(userId,projectId);
        } catch (SQLException e) {
            connection.rollback();
            throw new Exception("Can't remove project");
        } finally {
            connection.close();
        }
    }

    @Override
    public void persist(@Nullable final Project project) throws Exception {
        if (project == null) return;
        @NotNull final Connection connection = getConnection();
        connection.setAutoCommit(false);
        @NotNull final IProjectRepository repository = new ProjectRepository(connection);
        try {
           repository.persist(project);
        } catch (SQLException e) {
            connection.rollback();
            throw new Exception("Can't create project");
        } finally {
            connection.close();
        }
    }

    @Override
    public void merge(@Nullable final Project project) throws Exception {
        if (project == null) return;
        @NotNull final Connection connection = getConnection();
        connection.setAutoCommit(false);
        @NotNull final IProjectRepository repository = new ProjectRepository(connection);
        try {
            repository.merge(project);
        } catch (SQLException e) {
            connection.rollback();
            throw new Exception("Can't update project");
        } finally {
            connection.close();
        }
    }

    @Override
    public void removeAll() throws Exception {
        @NotNull final Connection connection = getConnection();
        @NotNull final IProjectRepository repository = new ProjectRepository(connection);
        connection.setAutoCommit(false);
        try {
            repository.removeAll();
        } catch (SQLException e) {
            connection.rollback();
            throw new Exception("Can't remove project");
        } finally {
            connection.close();
        }
    }

    @Nullable
    @Override
    public String findIdByName(@Nullable final String userId, @Nullable final String name) throws Exception {
        if (name == null || name.isEmpty()) return null;
        if (userId == null || userId.isEmpty()) return null;
        @NotNull final Connection connection = getConnection();
        @NotNull final IProjectRepository repository = new ProjectRepository(connection);
        @Nullable final String id = repository.findIdByName(userId, name);
        connection.close();
        return id;
    }

    @NotNull
    @Override
    public List<Project> findAll(@Nullable final String userId) throws Exception {
        if (userId == null || userId.isEmpty()) return new ArrayList<>();
        @NotNull final Connection connection = getConnection();
        @NotNull final IProjectRepository repository = new ProjectRepository(connection);
        @NotNull final List<Project>  projects= repository.findAll(userId);
        connection.close();
        return projects;
    }

    @Override
    public void removeAll(@Nullable final String userId) throws Exception {
        if (userId == null || userId.isEmpty()) return;
        @NotNull final Connection connection = getConnection();
        @NotNull final IProjectRepository repository = new ProjectRepository(connection);
        connection.setAutoCommit(false);
        try {
            repository.removeAll(userId);
        } catch (SQLException e) {
            connection.rollback();
            throw new Exception("Can't remove project");
        } finally {
            connection.close();
        }
    }

    @NotNull
    @Override
    public Collection<Project> sortAll(@Nullable final String userId, @Nullable String sortRequest) throws Exception {
        @NotNull String sortType = Sort.SORT_ASC;
        if (userId == null || userId.isEmpty()) return Collections.emptyList();
        if (sortRequest == null || sortRequest.isEmpty()) return sortByCreationDate(userId);
        if (sortRequest.toUpperCase().contains(Sort.SUFFIX)) {
            sortRequest = sortRequest.substring(0, sortRequest.length() - Sort.SUFFIX.length());
            sortType = Sort.SORT_DESC;
        }
        switch (sortRequest.toLowerCase()) {
            case "start date":
                return sortByStartDate(userId, sortType);
            case "finish date":
                return sortByFinishDate(userId, sortType);
            case "status":
                return sortByStatus(userId, sortType);
            default:
                throw new IllegalSortTypeException("cannot sort");
        }
    }

    private Collection<Project> sortByStartDate(@Nullable final String userId, @Nullable String sortType) throws Exception {
        if (userId == null || userId.isEmpty()) return Collections.emptyList();
        if (sortType == null || sortType.isEmpty()) sortType = "ASC";
        @NotNull final Connection connection = getConnection();
        @NotNull final IProjectRepository repository = new ProjectRepository(connection);
        @NotNull final Collection<Project> projects= repository.sortAllByStartDate(userId,sortType);
        connection.close();
        return projects;
    }

    private Collection<Project> sortByFinishDate(@Nullable final String userId, @Nullable String sortType) throws Exception {
        if (userId == null || userId.isEmpty()) return Collections.emptyList();
        if (sortType == null || sortType.isEmpty()) sortType = "ASC";
        @NotNull final Connection connection = getConnection();
        @NotNull final IProjectRepository repository = new ProjectRepository(connection);
        @NotNull final Collection<Project> projects= repository.sortAllByFinishDate(userId,sortType);
        connection.close();
        return projects;
    }

    private Collection<Project> sortByStatus(@Nullable final String userId, @Nullable String sortType) throws Exception {
        if (userId == null || userId.isEmpty()) return Collections.emptyList();
        if (sortType == null || sortType.isEmpty()) sortType = "ASC";
        @NotNull final Connection connection = getConnection();
        @NotNull final IProjectRepository repository = new ProjectRepository(connection);
        @NotNull final Collection<Project> projects= repository.sortAllByStatus(userId,sortType);
        connection.close();
        return projects;
    }

    private Collection<Project> sortByCreationDate(@Nullable final String userId) throws Exception {
        if (userId == null || userId.isEmpty()) return Collections.emptyList();
        @NotNull final Connection connection = getConnection();
        @NotNull final IProjectRepository repository = new ProjectRepository(connection);
        @NotNull final Collection<Project> projects= repository.sortAllByCreationDate(userId);
        connection.close();
        return projects;
    }

    @Override
    @NotNull
    public Collection<Project> searchByName(@Nullable final String userId, @Nullable final String string) throws Exception {
        if (userId == null || userId.isEmpty()) return Collections.emptyList();
        if (string == null || string.isEmpty()) return Collections.emptyList();
        @NotNull final Connection connection = getConnection();
        @NotNull final IProjectRepository repository = new ProjectRepository(connection);
        @NotNull final Collection<Project> projects= repository.searchByName(userId,string);
        connection.close();
        return projects;
    }

    @Override
    @NotNull
    public Collection<Project> searchByDescription(@Nullable final String userId, @Nullable final String string) throws Exception {
        if (userId == null || userId.isEmpty()) return Collections.emptyList();
        if (string == null || string.isEmpty()) return Collections.emptyList();
        @NotNull final Connection connection = getConnection();
        @NotNull final IProjectRepository repository = new ProjectRepository(connection);
        @NotNull final Collection<Project> projects= repository.searchByDescription(userId,string);
        connection.close();
        return projects;
    }

}