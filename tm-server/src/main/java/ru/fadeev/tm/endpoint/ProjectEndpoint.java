package ru.fadeev.tm.endpoint;

import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.fadeev.tm.api.endpoint.IProjectEndpoint;
import ru.fadeev.tm.api.service.ServiceLocator;
import ru.fadeev.tm.entity.Project;
import ru.fadeev.tm.entity.Session;
import ru.fadeev.tm.enumerated.Role;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import java.util.Collection;
import java.util.List;

@Setter
@WebService(endpointInterface = "ru.fadeev.tm.api.endpoint.IProjectEndpoint")
public final class ProjectEndpoint extends AbstractEndpoint implements IProjectEndpoint {

    public ProjectEndpoint() {
        super(null);
    }

    public ProjectEndpoint(@NotNull final ServiceLocator serviceLocator) {
        super(serviceLocator);
    }

    @Override
    @NotNull
    @WebMethod
    public List<Project> findAllProjectAdmin(@WebParam(name = "session") @Nullable final Session session) throws Exception {
        serviceLocator.getSessionService().checkSession(session, Role.ADMINISTRATOR);
        return serviceLocator.getProjectService().findAll();
    }

    @Override
    @Nullable
    @WebMethod
    public Project findOneProject(
            @WebParam(name = "session") @Nullable final Session session,
            @WebParam(name = "projectId") @Nullable final String projectId
    ) throws Exception {
        serviceLocator.getSessionService().checkSession(session);
        return serviceLocator.getProjectService().findOne(session.getUserId(), projectId);
    }

    @Override
    @Nullable
    @WebMethod
    public Project removeProject(
            @WebParam(name = "session") @Nullable final Session session,
            @WebParam(name = "id") @Nullable final String id
    ) throws Exception {
        serviceLocator.getSessionService().checkSession(session);
        serviceLocator.getProjectService().remove(session.getUserId(), id);
        return null;
    }

    @Override
    @WebMethod
    public void persistProject(
            @WebParam(name = "session") @Nullable final Session session,
            @WebParam(name = "project") @Nullable final Project project
    ) throws Exception {
        serviceLocator.getSessionService().checkSession(session);
        serviceLocator.getProjectService().persist(project);
    }

    @Override
    @WebMethod
    public void mergeProject(
            @WebParam(name = "session") @Nullable final Session session,
            @WebParam(name = "project") @Nullable final Project project
    ) throws Exception {
        serviceLocator.getSessionService().checkSession(session);
        serviceLocator.getProjectService().merge(project);
    }

    @Override
    @WebMethod
    public void removeAllProjectAdmin(
            @WebParam(name = "session") @Nullable final Session session
    ) throws Exception {
        serviceLocator.getSessionService().checkSession(session, Role.ADMINISTRATOR);
        serviceLocator.getProjectService().removeAll();
    }

    @Override
    @Nullable
    @WebMethod
    public String findIdByNameProject(
            @WebParam(name = "session") @Nullable final Session session,
            @WebParam(name = "name") @Nullable final String name
    ) throws Exception {
        serviceLocator.getSessionService().checkSession(session);
        return serviceLocator.getProjectService().findIdByName(session.getUserId(), name);
    }

    @Override
    @NotNull
    @WebMethod
    public List<Project> findAllProject(@WebParam(name = "session") @Nullable final Session session) throws Exception {
        serviceLocator.getSessionService().checkSession(session);
        return serviceLocator.getProjectService().findAll(session.getUserId());
    }

    @Override
    @WebMethod
    public void removeAllProject(@WebParam(name = "session") @Nullable final Session session) throws Exception {
        serviceLocator.getSessionService().checkSession(session);
        serviceLocator.getProjectService().removeAll(session.getUserId());
    }

    @NotNull
    @Override
    public Collection<Project> sortAllProject(
            @WebParam(name = "session") @Nullable final Session session,
            @WebParam(name = "selectedSort") @Nullable final String selectedSort) throws Exception {
        serviceLocator.getSessionService().checkSession(session);
        return serviceLocator.getProjectService().sortAll(session.getUserId(), selectedSort);
    }


    @NotNull
    @Override
    @WebMethod
    public Collection<Project> searchByNameProject(
            @WebParam(name = "session") @Nullable final Session session,
            @WebParam(name = "string") @Nullable final String string
    ) throws Exception {
        System.out.println(session);
        System.out.println(string);
        serviceLocator.getSessionService().checkSession(session);
        return serviceLocator.getProjectService().searchByName(session.getUserId(), string);
    }

    @NotNull
    @Override
    @WebMethod
    public Collection<Project> searchByDescriptionProject(
            @WebParam(name = "session") @Nullable final Session session,
            @WebParam(name = "string") @Nullable final String string
    ) throws Exception {
        serviceLocator.getSessionService().checkSession(session);
        return serviceLocator.getProjectService().searchByDescription(session.getUserId(), string);
    }

}