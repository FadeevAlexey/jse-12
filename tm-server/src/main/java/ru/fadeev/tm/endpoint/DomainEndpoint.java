package ru.fadeev.tm.endpoint;

import org.jetbrains.annotations.Nullable;
import ru.fadeev.tm.api.endpoint.IDomainEndpoint;
import ru.fadeev.tm.api.service.ServiceLocator;
import ru.fadeev.tm.entity.Session;
import ru.fadeev.tm.enumerated.Role;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;

@WebService(endpointInterface = "ru.fadeev.tm.api.endpoint.IDomainEndpoint")
public class DomainEndpoint extends AbstractEndpoint implements IDomainEndpoint {

    public DomainEndpoint() {
        super(null);
    }

    public DomainEndpoint(ServiceLocator serviceLocator) {
        super(serviceLocator);
    }

    @Override
    @WebMethod
    public void saveToBinary(@WebParam(name = "session") @Nullable final Session session) throws Exception {
        serviceLocator.getSessionService().checkSession(session, Role.ADMINISTRATOR);
        serviceLocator.getDomainService().saveToBinary();
    }

    @Override
    @WebMethod
    public void loadFromBinary(@WebParam(name = "session") @Nullable final Session session) throws Exception {
        serviceLocator.getSessionService().checkSession(session, Role.ADMINISTRATOR);
        serviceLocator.getDomainService().loadFromBinary();
    }

    @Override
    @WebMethod
    public void saveToJsonFasterXml(@WebParam(name = "session") @Nullable final Session session) throws Exception {
        serviceLocator.getSessionService().checkSession(session, Role.ADMINISTRATOR);
        serviceLocator.getDomainService().saveToJsonFasterXml();
    }

    @Override
    @WebMethod
    public void loadFromJsonFasterXml(@WebParam(name = "session") @Nullable final Session session) throws Exception {
        serviceLocator.getSessionService().checkSession(session, Role.ADMINISTRATOR);
        serviceLocator.getDomainService().loadFromJsonFasterXml();
    }

    @Override
    @WebMethod
    public void saveToJsonJaxb(@WebParam(name = "session") @Nullable final Session session) throws Exception {
        serviceLocator.getSessionService().checkSession(session, Role.ADMINISTRATOR);
        serviceLocator.getDomainService().saveToJsonJaxb();
    }

    @Override
    @WebMethod
    public void loadFromJsonJaxb(@WebParam(name = "session") @Nullable final Session session) throws Exception  {
        serviceLocator.getSessionService().checkSession(session, Role.ADMINISTRATOR);
        serviceLocator.getDomainService().loadFromJsonJaxb();
    }

    @Override
    @WebMethod
    public void saveToXmlJaxb(@WebParam(name = "session") @Nullable final Session session) throws Exception {
        serviceLocator.getSessionService().checkSession(session, Role.ADMINISTRATOR);
        serviceLocator.getDomainService().saveToXmlJaxb();
    }

    @Override
    @WebMethod
    public void loadFromXmlJaxb(@WebParam(name = "session") @Nullable final Session session) throws Exception {
        serviceLocator.getSessionService().checkSession(session, Role.ADMINISTRATOR);
        serviceLocator.getDomainService().loadFromXmlJaxb();
    }

    @Override
    @WebMethod
    public void saveToXmlFasterxml(@WebParam(name = "session") @Nullable final Session session) throws Exception {
        serviceLocator.getSessionService().checkSession(session, Role.ADMINISTRATOR);
        serviceLocator.getDomainService().saveToXmlFasterxml();
    }

    @Override
    @WebMethod
    public void loadFromXmlFasterxml(@WebParam(name = "session") @Nullable final Session session) throws Exception {
        serviceLocator.getSessionService().checkSession(session, Role.ADMINISTRATOR);
        serviceLocator.getDomainService().loadFromXmlFasterxml();
    }

}
