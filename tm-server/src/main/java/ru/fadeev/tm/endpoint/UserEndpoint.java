package ru.fadeev.tm.endpoint;

import lombok.Setter;
import org.jetbrains.annotations.Nullable;
import ru.fadeev.tm.api.endpoint.IUserEndpoint;
import ru.fadeev.tm.api.service.ServiceLocator;
import ru.fadeev.tm.entity.Session;
import ru.fadeev.tm.entity.User;
import ru.fadeev.tm.enumerated.Role;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import java.util.List;

@Setter
@WebService(endpointInterface = "ru.fadeev.tm.api.endpoint.IUserEndpoint")
public class UserEndpoint extends AbstractEndpoint implements IUserEndpoint {

    public UserEndpoint(){
        super(null);
    }

    public UserEndpoint(ServiceLocator serviceLocator) {
        super(serviceLocator);
    }

    @Override
    @WebMethod
    public List<User> findAllUser(@WebParam(name = "session") @Nullable final Session session) throws Exception {
        serviceLocator.getSessionService().checkSession(session, Role.ADMINISTRATOR);
       return serviceLocator.getUserService().findAll();
    }

    @Override
    @WebMethod
    public @Nullable User findOneUser(
            @WebParam(name = "session") @Nullable final Session session
    ) throws Exception {
        serviceLocator.getSessionService().checkSession(session);
        return serviceLocator.getUserService().findOne(session.getUserId());
    }

    @Override
    @WebMethod
    public @Nullable User removeUser(
            @WebParam(name = "session") @Nullable final Session session,
            @WebParam(name = "id") @Nullable final String id
    ) throws Exception {
        serviceLocator.getSessionService().checkSession(session,Role.ADMINISTRATOR);
        serviceLocator.getUserService().remove(id);
        return null;
    }

    @Override
    @WebMethod
    public void persistUser(
            @WebParam(name = "user") final @Nullable User user
    ) throws Exception {
        if (user == null) return;
        if (user.getRole() == Role.ADMINISTRATOR) return;
        serviceLocator.getUserService().persist(user);
    }

    @Override
    @WebMethod
    public void mergeUser(
            @WebParam(name = "session") @Nullable final Session session,
            @WebParam(name = "user") final @Nullable User user
    ) throws Exception {
        serviceLocator.getSessionService().checkSession(session);
        if (user == null) return;
        if (user.getRole() == Role.ADMINISTRATOR && session.getRole() != Role.ADMINISTRATOR) return;
        serviceLocator.getUserService().merge(user);
    }

    @Override
    @WebMethod
    public void removeAllUser(
            @WebParam(name = "session") @Nullable final Session session
    ) throws Exception {
        serviceLocator.getSessionService().checkSession(session, Role.ADMINISTRATOR);
        serviceLocator.getUserService().removeAll();
    }

    @Override
    @WebMethod
    public boolean isLoginExistUser(
            @WebParam(name = "login") @Nullable final String login) throws Exception {
        return serviceLocator.getUserService().isLoginExist(login);
    }

    @Nullable
    @Override
    @WebMethod
    public User findUserByLoginUser(
            @WebParam(name = "session") @Nullable final Session session,
            @WebParam(name = "login") @Nullable final String login
    ) throws Exception {
        serviceLocator.getSessionService().checkSession(session,Role.ADMINISTRATOR);
        return serviceLocator.getUserService().findUserByLogin(login);
    }

    @Override
    @WebMethod
    public void setAdminRole(
            @WebParam(name = "session") @Nullable final Session session,
            @WebParam(name = "user") @Nullable final User user) throws Exception {
        serviceLocator.getSessionService().checkSession(session, Role.ADMINISTRATOR);
        serviceLocator.getUserService().setAdminRole(user);
    }

}