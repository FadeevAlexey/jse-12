package ru.fadeev.tm.api.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.fadeev.tm.entity.Session;
import ru.fadeev.tm.entity.Task;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import java.util.Collection;
import java.util.List;

@WebService
public interface ITaskEndpoint {

    @NotNull
    @WebMethod
    List<Task> findAllTaskAdmin(@WebParam(name = "session") @Nullable Session session) throws Exception;

    @Nullable
    @WebMethod
    Task findOneTask(
            @WebParam(name = "session") @Nullable Session session,
            @WebParam(name = "id") @Nullable String id
    ) throws Exception;

    @Nullable
    @WebMethod
    Task removeTask(
            @WebParam(name = "session") @Nullable Session session,
            @WebParam(name = "id") @Nullable String id
    ) throws Exception;

    @WebMethod
    void persistTask(
            @WebParam(name = "session") @Nullable Session session,
            @WebParam(name = "task") @Nullable Task task
    ) throws Exception;

    @WebMethod
    void mergeTask(
            @WebParam(name = "session") @Nullable Session session,
            @WebParam(name = "task") @Nullable Task task
    ) throws Exception;

    @WebMethod
    void removeAllTaskAdmin(
            @WebParam(name = "session") @Nullable Session session
    ) throws Exception;

    @Nullable
    @WebMethod
    String findIdByNameTask(
            @WebParam(name = "session") @Nullable Session session,
            @WebParam(name = "name") @Nullable String name
    ) throws Exception;

    @NotNull
    @WebMethod
    Collection<Task> findAllTask(@WebParam(name = "session") @Nullable Session session) throws Exception;

    @WebMethod
    void removeAllTask(@WebParam(name = "session") @Nullable Session session) throws Exception;

    @NotNull
    Collection<Task> sortAllTask(
            @WebParam(name = "session") @Nullable Session session,
            @WebParam(name = "selectedSort") @Nullable String selectedSort) throws Exception;

    @NotNull
    @WebMethod
    Collection<Task> searchByNameTask(
            @WebParam(name = "session") @Nullable Session session,
            @WebParam(name = "string") @Nullable String string
    ) throws Exception;

    @NotNull
    @WebMethod
    Collection<Task> searchByDescriptionTask(
            @WebParam(name = "session") @Nullable Session session,
            @WebParam(name = "string") @Nullable String string
    ) throws Exception;

    @NotNull
    @WebMethod
    Collection<Task> findAllByProjectIdTask(
            @WebParam(name = "session") @Nullable Session session,
            @WebParam(name = "projectId") @Nullable String projectId
    ) throws Exception;

    @WebMethod
    void removeAllByProjectIdTask(
            @WebParam(name = "session") @Nullable Session session,
            @WebParam(name = "projectId") @Nullable String projectId
    ) throws Exception;

    @WebMethod
    void removeAllProjectTask(@WebParam(name = "session") @Nullable Session session) throws Exception;

}