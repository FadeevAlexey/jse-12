package ru.fadeev.tm.api.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.fadeev.tm.entity.Project;

import java.sql.SQLException;
import java.util.Collection;
import java.util.List;

public interface IProjectRepository extends IRepository<Project> {

    void removeAll(@NotNull String userId) throws SQLException;

    @NotNull
    List<Project> findAll(@NotNull String userId) throws SQLException;

    @Nullable
    String findIdByName(@NotNull String userId, @NotNull String name) throws SQLException;

    @NotNull
    Collection<Project> searchByName(@NotNull String userId, @NotNull String string) throws SQLException;

    @NotNull
    Collection<Project> searchByDescription(@NotNull String userId, @NotNull String string) throws SQLException;

    @NotNull
    Collection<Project> sortAllByStartDate(@NotNull String userId, @NotNull String sortType) throws SQLException;

    @NotNull
    Collection<Project> sortAllByFinishDate(@NotNull String userId, @NotNull String sortType) throws SQLException;

    @NotNull
    Collection<Project> sortAllByStatus(@NotNull String userId, @NotNull String sortType) throws SQLException;

    @NotNull
    Collection<Project> sortAllByCreationDate(@NotNull String userId) throws SQLException;

    @Nullable
    Project findOne(@NotNull String userId, @NotNull String id) throws SQLException;

    void remove(@NotNull String userId, @NotNull String projectId) throws SQLException;

}
