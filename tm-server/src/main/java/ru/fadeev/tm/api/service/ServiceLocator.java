package ru.fadeev.tm.api.service;

import org.jetbrains.annotations.NotNull;
import ru.fadeev.tm.api.endpoint.*;

public interface ServiceLocator {

    @NotNull
    IProjectService getProjectService();

    @NotNull
    ITaskService getTaskService();

    @NotNull
    IUserService getUserService();

    @NotNull
    IDomainService getDomainService();

    @NotNull
    ISessionService getSessionService();

    @NotNull
    IProjectEndpoint getProjectEndpoint();

    @NotNull
    IUserEndpoint getUserEndpoint();

    @NotNull
    ITaskEndpoint getTaskEndpoint();

    @NotNull
    ISessionEndpoint getSessionEndpoint();

    @NotNull
    IDomainEndpoint getDomainEndpoint();

    @NotNull
    IPropertyService getPropertyService();

}