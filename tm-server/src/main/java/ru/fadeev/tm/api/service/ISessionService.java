package ru.fadeev.tm.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.fadeev.tm.entity.Session;
import ru.fadeev.tm.enumerated.Role;

public interface ISessionService extends IService<Session> {

    @Nullable
    Session openSession(@Nullable String login, @Nullable String password) throws Exception;

    void closeSession(@Nullable Session session) throws Exception;

    boolean contains(@Nullable String sessionId) throws Exception;

    void checkSession(@Nullable Session currentSession) throws Exception;

    void checkSession(@Nullable Session currentSession, @NotNull final Role role) throws Exception;

}