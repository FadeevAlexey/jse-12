package ru.fadeev.tm.api.repository;

import org.jetbrains.annotations.NotNull;
import ru.fadeev.tm.entity.Session;

import java.sql.SQLException;

public interface ISessionRepository extends IRepository<Session> {

    Session findByUserId(@NotNull String userId) throws SQLException;

    void removeSessionBySignature(@NotNull String signature) throws SQLException;

    boolean contains(@NotNull String sessionId) throws SQLException;
}