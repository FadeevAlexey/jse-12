package ru.fadeev.tm.api.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.sql.SQLException;
import java.util.List;

public interface IRepository<E> {

    @NotNull
    List<E> findAll() throws SQLException;

    @Nullable
    E findOne(@NotNull String id) throws SQLException;

    void remove(@NotNull String id) throws SQLException;

    void persist(E e) throws Exception;

    void merge(@NotNull E e) throws Exception;

    void removeAll() throws SQLException;

}