package ru.fadeev.tm.api.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.fadeev.tm.entity.Task;

import java.sql.SQLException;
import java.util.Collection;

public interface ITaskRepository extends IRepository<Task> {

    void removeAll(@NotNull String userId) throws SQLException;

    @NotNull
    Collection<Task> findAll(@NotNull String userId) throws SQLException;

    @NotNull
    Collection<Task> findAllByProjectId(@NotNull String projectId, @NotNull String userId) throws SQLException;

    @Nullable
    String findIdByName(@NotNull String userId, @NotNull String name) throws SQLException;

    void removeAllByProjectId(@NotNull String userId, @NotNull String projectId) throws SQLException;

    void removeAllProjectTask(@NotNull String userId) throws SQLException;

    @NotNull
    Collection<Task> searchByName(@NotNull String userId, @NotNull String string) throws SQLException;

    @NotNull
    Collection<Task> searchByDescription(@NotNull String userId, @NotNull String string) throws SQLException;

    @Nullable
    Task findOne(@NotNull String userId, @NotNull String id) throws SQLException;

    void remove(@NotNull String userId, @NotNull String taskId) throws SQLException;

    @NotNull
    Collection<Task> sortAllByStartDate(@NotNull String userId, @NotNull String sortType) throws SQLException;

    @NotNull
    Collection<Task> sortAllByFinishDate(@NotNull String userId, @NotNull String sortType) throws SQLException;

    @NotNull
    Collection<Task> sortAllByStatus(@NotNull String userId, @NotNull String sortType) throws SQLException;

    @NotNull
    Collection<Task> sortAllByCreationDate(@NotNull String userId) throws SQLException;

}